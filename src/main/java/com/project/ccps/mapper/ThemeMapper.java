package com.project.ccps.mapper;

import com.project.ccps.dto.ThemeDto;
import com.project.ccps.model.Theme;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ThemeMapper {

    int  create(Theme theme);

    int edit(Theme theme);

    int  delete(Integer  id);

    List<Theme> selectAll(@Param( "value" ) String sqlValue);//注意：带 "sql" 都注入不了

    int queryNode(Integer id);

    int  queryByThemeName(@Param( "name" ) String name);

    int queryArticleCount(Integer id);

    int queryIfCheck(@Param( "paramId" ) Integer paramId);

    int queryThemeCount(@Param( "name" )String name, @Param( "id" )Integer id);

    List<Theme> queryThemeByRoleId(int roleId);

    void insertThemeUser(@Param( "themeId" ) int themeId,@Param( "userId" ) Integer userId,@Param( "userName" )String userName);

    int delByTheme(Integer id);

    List<Integer> queyUserByThemeId(Integer id);

    String checkUserName(Integer id);

    void insertThemeUserName(int themeId, String userName);

    List<ThemeDto> selectSiteTheme();

    List<Theme> selectByThemeId(int themeId);

    int selectSub(int id);
}

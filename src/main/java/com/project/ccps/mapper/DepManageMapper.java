package com.project.ccps.mapper;

import com.project.ccps.model.DepGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DepManageMapper {

    int addDepGroup(DepGroup depGroup);

    int queryGroupByName(String groupName);

    List<DepGroup> queryDepGroup(@Param( "groupName" ) String groupName);
}

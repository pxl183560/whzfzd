package com.project.ccps.mapper;

import com.project.ccps.dto.SysLogDto;
import com.project.ccps.model.SysLog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysLogMapper {

    void saveSysLog(SysLog sysLog);

    List<SysLog> selectAll(SysLogDto sysLogDto);

    int selectAllCount(SysLogDto sysLogDto);
}

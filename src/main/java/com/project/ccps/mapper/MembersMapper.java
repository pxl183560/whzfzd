package com.project.ccps.mapper;

import com.project.ccps.model.Members;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MembersMapper {
    void save(Members members);
}

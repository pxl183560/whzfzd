package com.project.ccps.mapper;

import com.project.ccps.dto.RoleAuthDto;
import com.project.ccps.model.OperateAuth;
import com.project.ccps.model.Page;
import com.project.ccps.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface UserAuthMapper {

    void addAuth(@Param( "id" ) Integer id,@Param( "tableName" ) String tableName,
                 @Param( "columnName" )String columnName,@Param( "roleId" )Integer roleId);

    List<RoleAuthDto> queryOperateByRoleId(Integer roleId);

    Integer addOperateAuth(OperateAuth operateAuth);

    void delRoleAuth(@Param( "tableName" ) String tableName, @Param( "roleId" ) Integer roleId);

    int operateRoleValue(int roleId);

    int editOperateAuth(OperateAuth operateAuth);

    OperateAuth rollOperateAuth(@Param( "roleId" ) String roleId);

    List<Integer> themeAuthList(Integer id);

    List<Integer>  menuAuthList(int roleId);

    Map<String, Integer> operateAuthMap(Integer roleId);

    List<Role> getRole(Page page);

    int selectUserCount();

    int queryLookAuthCount(int roleId);

    void updateLookAuth(@Param( "articleLookStr" ) String articleLookStr,@Param( "roleId" ) int roleId);

    void insertLookAuth(@Param( "articleLookStr" ) String articleLookStr,@Param( "roleId" ) int roleId);
}

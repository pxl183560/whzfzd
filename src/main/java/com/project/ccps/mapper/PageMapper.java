package com.project.ccps.mapper;

import com.project.ccps.model.Common;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PageMapper {
    List<Map<String,String>> selectArticle(Integer secondTheme,Integer showNumber);

    List<Map<String,String>>  selectArticleImage(Integer secondTheme);

    List<Map<String, String>> selectTemplate(Integer columnNumber);

    List<Map<String, String>> articleTitle(Integer secondThemeId);

    String articleContext(Integer articleId);

    List<Map<String,String>> selectValue(Common common);

    int selectValueCount(Common common);

    List<Map<String, String>> selectDesc(int  secondThemeId,int  len);

    List<Map<String, String>> selectArticleImageSigle(int secondThemeId);

    Map<String, String> secondThemeName(Integer secondThemeId);

    Map<String,String> firstThemeName(Integer firstThemeId);

    List<Map<String, String>> secondName(Integer firstThemeId);

    List<Map<String, String>> selectArticleImage3(int parseInt);

    List<Map<String, String>> selectArticleImage8(int parseInt);
}

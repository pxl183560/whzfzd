package com.project.ccps.mapper;

import com.project.ccps.dto.ArticleByIdDto;
import com.project.ccps.dto.ArticleDto;
import com.project.ccps.dto.MeasureDto;
import com.project.ccps.model.Article;
import com.project.ccps.model.Comment;
import com.project.ccps.model.Reply;
import com.project.ccps.vo.ArticleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ArticleMapper {

    int  create(Article article);

    int selectAllCount(ArticleVo articleVo);

    List<ArticleDto> selectAll(@Param( "idList" ) List<Integer>  idList);

    List<Map<String,Object>>  selectColumn();

    int edit(Article article);

    Map<String,String> selectById(Integer id);

   Map<String,Object> selectImageSize(@Param("columNumber") Integer columNumber, @Param("position") Integer position);

   List<Article> findGoodArticle();

   List<MeasureDto> findAllMeasure(Integer secondThemeId);

    Article findArticleById(Integer id);

    int editArticle(Article article);

    int  deletes(@Param("ids") String ids);

    int selectByArticelName(@Param( "title" ) String title,@Param( "themeId" ) Integer themeId);

    int themeChildCount(Integer themeId);

    void priority(@Param( "idStr" ) String idStr);

    void updatePriority();

    void updateTop(Integer themeId);

    void top(@Param( "id" ) Integer id);

    int selectCountByName(Article article);

    List<Article> selectArticleByIds(@Param( "articleIds" )String articleIds);

    void delByIds(@Param( "articleIds" ) String articleIds);

    int backout(@Param( "articleId" ) int articleId,@Param( "checkStatus" ) int checkStatus);

    int rollBack(@Param( "articleId" ) int articleId,
                 @Param( "backReason" ) String backReason,
                 @Param( "checkUserName" ) String checkUserName,
                 @Param( "checkUserId" ) int  checkUserId
                 );

    int sendArticle(@Param( "articleId" ) int articleId,
                    @Param( "checkUserName" ) String checkUserName,
                    @Param( "checkUserId" ) int  checkUserId);

    int checkArticleCount(ArticleVo articleVo);

    List<ArticleDto> checkArticle(ArticleVo articleVo);

    int selectTransAllCount(ArticleVo articleVo);

    List<ArticleDto> selectTransAll(ArticleVo articleVo);

    ArticleByIdDto getArticleById(int id);

    List<Map<String,String>> getNewArticle(@Param( "themeId" ) Integer id);

    int selectArticleByThemeIdCount(ArticleVo articleVo);

    List<ArticleDto> selectArticleByThemeId(ArticleVo articleVo);

    List<Article> queryArticleContent();

    void updateContent(@Param( "content" ) String content, @Param( "id" ) Integer id);

    int readCount(int articleId);

    Integer getInteractionId();

    List<Comment> selectComment( Integer id);

    Integer selectFrontId(@Param( "id" ) Integer id, @Param( "themeId" ) Integer themeId);

    Integer selectLaterId(@Param( "id" ) Integer id, @Param( "themeId" ) Integer themeId);

    List<Reply> selectReply(Integer id);

    int comment(Comment comment);

    int reply(Reply reply);

    int revocation(Integer id);

    String getThemeIdStr(Integer themeId);

    List<Map<String, Object>> depArticleStatistics(@Param( "startTime" ) String startTime,
                                                   @RequestParam("endTime") String endTime,
                                                   @RequestParam("themeIdStr") String themeIdStr);

    List<ArticleDto> topArticle(ArticleVo articleVo);

    String getTimeParam(ArticleVo articleVo);

    List<Integer> idList(ArticleVo articleVo);

    String topArticleId(ArticleVo articleVo);
}

package com.project.ccps.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FileMapper {

    String selectFilePath(String id);

    void delFile(String id);

    long articleMaxTime();

    List<String> articleFileReqUrl();
}

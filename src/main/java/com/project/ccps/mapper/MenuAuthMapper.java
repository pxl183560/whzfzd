package com.project.ccps.mapper;
import com.project.ccps.model.MenuAuth;
import com.project.ccps.model.Theme;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MenuAuthMapper {

    int  create(MenuAuth menuAuth);

    int edit(MenuAuth menuAuth);

    int  delete(Integer id);

    List<MenuAuth> selectAll(@Param( "valueSql" ) String valueSql);

    int queryNode(Integer id);

    int queryByMenuName(@Param("menuName") String menuName);

    int queryMenuNameCount(@Param("menuName") String menuName, @Param("id") Integer id);

    void insertMenuRole(@Param( "menuId" ) Integer menuId,@Param( "roleId" ) Integer roleId);

    void deleteMenuRole(Integer menuId);

    List<MenuAuth> queryMenuByRoleId(int roleId);
}

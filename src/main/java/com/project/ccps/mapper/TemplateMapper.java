package com.project.ccps.mapper;

import com.project.ccps.dto.TemplateDto;
import com.project.ccps.model.Page;
import com.project.ccps.model.Template;
import com.project.ccps.model.WebsiteViewCount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TemplateMapper {

    int selectAllCount();

    List<Template> selectAll(Page page);

    int addTemplateDeploy(Template templete);

    List<TemplateDto> template1(Integer tempId);

    List<Map<String, Object>> queryCoverImg(@Param( "themeId" ) Integer themeId,@Param( "limitCount" ) Integer limitCount);

    List<Map<String, Object>> articleTitle(@Param( "themeId" ) Integer themeId,@Param( "showNumber" ) Integer showNumber);

    List<Map<String, Object>> queryCoverVideo();

    List<Map<String, Object>> subjects();

    void websiteView();

    WebsiteViewCount websiteViewCount();
}

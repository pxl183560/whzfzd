package com.project.ccps.exception;
import org.springframework.http.HttpStatus;

public class ParameterServiceException extends ServiceException {
    private static final long serialVersionUID = 8362753245631601878L;

    public ParameterServiceException(String errorCode, String message) {
        super(errorCode, message);
        this.statusCode = HttpStatus.UNPROCESSABLE_ENTITY;
    }
}


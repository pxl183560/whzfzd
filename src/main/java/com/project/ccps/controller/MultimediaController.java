package com.project.ccps.controller;

import com.project.ccps.service.MulitimediaService;
import com.project.ccps.utils.JSONResult;
import com.project.ccps.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/multimedia")
public class MultimediaController {
    @Autowired
    MulitimediaService mulitimediaService;


    @RequestMapping(value = "/upload", method={RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public JSONResult upload(@RequestParam(value = "file", required = false) MultipartFile multipartFile,HttpServletRequest request) throws IOException {

        JSONResult result = mulitimediaService.upload(multipartFile,request);
        return result;
    }

    @RequestMapping(value="read" )
    @ResponseBody
    public void reading(HttpServletResponse response, @RequestParam(value = "caseId",required = false) String caseId) {
        JSONResult jsonResult = new JSONResult();
        try {
            String filePath = "E:\\upload\\"+ File.separator+"video.avi";
            File file = new File(filePath);
            InputStream fis = new BufferedInputStream(new FileInputStream(file));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Length", "" + file.length());
            response.setContentType("video/mp4;charset=UTF-8");
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @PostMapping("/uploadFiles")
    public  List<String> uploadFiles(List<MultipartFile> files) throws IOException {
        String path = "g:\\image\\";
        List<String> msgs = new ArrayList<>();
        if (files.size() < 1) {
            msgs.add("file_empty");
            return msgs;
        }
        for (int i = 0; i < files.size(); ++i) {
            MultipartFile file = files.get(i);
            if (!file.isEmpty()) {
                String filename = file.getOriginalFilename();
                String type = filename.indexOf(".") != -1
                        ? filename.substring(filename.lastIndexOf("."), filename.length())
                        : null;
                if (type == null) {
                    msgs.add("file_empty");
                    return msgs;
                }

                if (!(".PNG".equals(type.toUpperCase()) || ".JPG".equals(type.toUpperCase()))) {
                    msgs.add("wrong_file_extension");
                    return msgs;
                }
            }
        }
        for (int i = 0; i < files.size(); ++i) {
            MultipartFile file = files.get(i);
            String filename = file.getOriginalFilename();
            String type = filename.indexOf(".") != -1 ? filename.substring(filename.lastIndexOf("."), filename.length())
                    : null;
            String filepath = path + new Date().getTime()+ type;
            File filesPath = new File(path);
            if (!filesPath.exists()) {
                filesPath.mkdir();
            }
            BufferedOutputStream out = null;
            type = filepath.indexOf(".") != -1 ? filepath.substring(filepath.lastIndexOf(".") + 1, filepath.length())
                    : null;
            try {
                out = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                out.write(file.getBytes());
                msgs.add(filepath);
            } catch (Exception e) {
                // 没有上传成功
                e.printStackTrace();
            } finally {
                out.flush();
                out.close();
            }
        }
        return msgs;
    }
    @PostMapping("/uploadSingleFiles")
    public  JSONResult uploadFiles(MultipartFile files) throws IOException {
        JSONResult jsonResult = new JSONResult();
        String path = "g:\\image\\";
        String url = "";
        List<String> msgs = new ArrayList<>();
        if (!files.isEmpty()) {
            String filename = files.getOriginalFilename();
            url = path+filename+new Date().getTime();
            String type = filename.indexOf(".") != -1
                    ? filename.substring(filename.lastIndexOf("."), filename.length())
                    : null;
            if (type == null) {
                jsonResult.setStatusCode(1);
                jsonResult.setMessage("file_empty");
                return jsonResult;
            }

            if (!(".PNG".equals(type.toUpperCase()) || ".JPG".equals(type.toUpperCase()))) {
                jsonResult.setStatusCode(1);
                jsonResult.setMessage("wrong_file_extension");
                return jsonResult;
            }
        }
        String filename = files.getOriginalFilename();
        String type = filename.indexOf(".") != -1 ? filename.substring(filename.lastIndexOf("."), filename.length())
                : null;
        String filepath = path + new Date().getTime()+ type;
        File filesPath = new File(path);
        if (!filesPath.exists()) {
            filesPath.mkdir();
        }
        BufferedOutputStream out = null;
        type = filepath.indexOf(".") != -1 ? filepath.substring(filepath.lastIndexOf(".") + 1, filepath.length())
                : null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
            out.write(files.getBytes());
            jsonResult.setStatusCode(0);
            jsonResult.setMessage(filepath);
        } catch (Exception e) {
            // 没有上传成功
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
        }
        return jsonResult;
    }
    @RequestMapping(value = "/multiUpload")
    public Response multiUpload(@RequestParam("video") MultipartFile[] multipartFile) throws IOException {
        List<String> pathList=new ArrayList<>();
        //保存文件的目录
        String savePath="D:"+File.separator+"upload"+File.separator;
        if(null!=multipartFile && multipartFile.length>0){
            //遍历并保存文件
            for(MultipartFile file:multipartFile){
                pathList.add(savePath+file.getOriginalFilename());
                file.transferTo(new File(savePath+file.getOriginalFilename()));
            }
        }
        return new Response("上传成功",200,pathList);
    }
}

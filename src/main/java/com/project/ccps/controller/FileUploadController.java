package com.project.ccps.controller;

import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.FileService;
import com.project.ccps.utils.DirFileName;
import com.project.ccps.utils.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/files")
public class FileUploadController {
    private static final Logger logger = LoggerFactory.getLogger(DepManageController.class);

    @Value("${file.FILE_LOCAL_URL}")
    private  String FILE_LOCAL_URL;
    @Value("${file.FILE_REQ_URL}")
    private  String FILE_REQ_URL;

    @Resource
    FileService fileService;

    @PostMapping(value = "/fileUpload")
    @CrossOrigin
    public JSONResult fileUpload(@RequestParam(value = "files") List<MultipartFile> files)  {
        JSONResult jsonResult = new JSONResult();
        List<String> reqList = new ArrayList<>(  );
        try{
            String newDir = new Date().getTime()+"/";
            for(MultipartFile multipartFile:files){
                String name =  multipartFile.getOriginalFilename();
                String localUrl = FILE_LOCAL_URL + newDir;
                File newFile = new File(localUrl);
                judeDirExists(newFile);
                String transUrl = localUrl + name;
                File newFile1 = new File(transUrl);
                multipartFile.transferTo(newFile1);
                String reqUrl = FILE_REQ_URL+newDir+name;
                reqList.add( reqUrl );
                jsonResult.setData( reqList );
            }
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
    public static void judeDirExists(File file) {
        if(!file.exists()&&!file.isDirectory()) {
            file.mkdirs();
        }
    }

    /*
     * 定时清理文章图片和视频文件
     * */
    //@Scheduled(cron="0 0 1 * * ? ")
    @GetMapping(value = "/imageVideoClean")
    public void imageVideoClean(){
        String fileUrl = FILE_LOCAL_URL;
        List<String> fileNameList = DirFileName.fileNames(fileUrl);
        long dbtime =  fileService.articleMaxTime();
        List<String> fileReqUrlList = fileService.articleFileReqUrl();
        List<String> dbFileNameList = new ArrayList<>(  );
        for(String reqName:fileReqUrlList){
            String[] reqNameArr = reqName.split( "," );
            for(String str:reqNameArr){
                if(!"".equals( str ) ){
                    String[] strArr = str.split( "/" );
                    dbFileNameList.add(strArr[strArr.length - 1]);
                }
            }
        }
        for(String fileName : fileNameList){
            int index = 0;
            for(String dbFileName:dbFileNameList){
                long time = Long.parseLong( fileName.split( "_" )[0] );
                if(fileName.equals( dbFileName ) && time <= dbtime){
                    index = 1;
                }
            }
            if(index == 0){
                File file = new File(fileUrl+fileName);
                file.delete();
            }
        }
    }

    //删除文件
    @GetMapping("/delFile")
    public JSONResult delFile(String id){
        JSONResult jsonResult = new JSONResult();
        try{
            String filePath = fileService.selectFilePath(id);
            fileService.delFile(id);
            File file = new File(filePath);
            file.delete();
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode() );
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg() );
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
}

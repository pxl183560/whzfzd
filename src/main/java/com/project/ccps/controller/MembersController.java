package com.project.ccps.controller;

import com.project.ccps.annotation.UserLoginToken;
import com.project.ccps.model.Members;
import com.project.ccps.service.MembersService;
import com.project.ccps.utils.Response;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("/members")
public class MembersController {
    @Resource
    private MembersService service;
    @UserLoginToken
    @PostMapping("/save")
    public Response save(@Valid @RequestBody Members members){
        service.save(members);
        return new Response("注册成功",200,null);
    }
}

package com.project.ccps.controller;

import com.project.ccps.model.DepGroup;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.DepManageService;
import com.project.ccps.utils.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dep")
public class DepManageController {
    private static final Logger logger = LoggerFactory.getLogger(DepManageController.class);

    @Autowired
    DepManageService depManageService;
    //添加
    @PostMapping("/addDepGroup")
    public JSONResult addDepGroup(@RequestBody DepGroup depGroup){
        JSONResult jsonResult = new JSONResult();
        try{
            int value  = depManageService.addDepGroup(depGroup);
            if(value > 0){
                if(value == ResultEnum.GROUPREPEAT.getCode()){
                    jsonResult.setStatusCode(ResultEnum.GROUPREPEAT.getCode());
                    jsonResult.setMessage( ResultEnum.GROUPREPEAT.getMsg());
                    return  jsonResult;
                }
                jsonResult.setStatusCode(ResultEnum.SUCCESS.getCode());
                jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
                return jsonResult;
            }
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    @GetMapping("/queryDepGroup")
    public JSONResult queryDepGroup(String groupName){
        JSONResult jsonResult = new JSONResult();
        try{
            List<DepGroup> depGroupList = depManageService.queryDepGroup(groupName);
            jsonResult.setData( depGroupList );
            jsonResult.setStatusCode(ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
}

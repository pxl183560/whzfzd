package com.project.ccps.controller;

import com.project.ccps.model.Common;
import com.project.ccps.service.PageService;
import com.project.ccps.utils.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/firstPage")
public class PageController {
    @Autowired
    PageService homePageService;
    @GetMapping("/template1")
    @CrossOrigin
    public Object template1(Integer columnNumber){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.template1(columnNumber));
        return  jsonResult;
    }

    @GetMapping("/template2")
    @CrossOrigin
    public Object template2(Integer columnNumber){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.template2(columnNumber));
        return jsonResult;
    }

    @GetMapping("/template3")
    @CrossOrigin
    public Object template3(Integer columnNumber){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.template3(columnNumber));
        return jsonResult;
    }
    @GetMapping("/template4")
    @CrossOrigin
    public Object template4(Integer columnNumber){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.template4(columnNumber));
        return jsonResult;
    }
    @GetMapping("/template5")
    @CrossOrigin
    public Object template5(Integer columnNumber){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.template5(columnNumber));
        return jsonResult ;
    }
    @GetMapping("/template6")
    @CrossOrigin
    public Object template6(Integer columnNumber){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.template6(columnNumber));
        return jsonResult ;
    }

    //点击文章跳转功能
    @GetMapping("/jumpArticle")
    @CrossOrigin
    public Object jumpArticle(@RequestParam("secondThemeId") Integer  secondThemeId,@RequestParam("articleId") Integer articleId){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.jumpArticle(secondThemeId,articleId));
        return jsonResult ;
    }
     //点击二级栏目跳转功能
    @GetMapping("/jumpSecondColumn")
    @CrossOrigin
    public Object jumpSecondColumn(@RequestParam("firstThemeId") Integer  secondThemeId,@RequestParam("secondThemeId") Integer articleId){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.jumpSecondColumn(secondThemeId,articleId));
        return jsonResult ;
    }

    //首页搜索功能
    @PostMapping("/selValue")
    @CrossOrigin
    public Object selectValue(@RequestBody Common commnon){
        commnon.setValue("'%"+commnon.getValue()+"%'");
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData(homePageService.selectValue(commnon));
        return jsonResult;
    }
}


package com.project.ccps.controller;

import com.project.ccps.model.MenuAuth;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.MenuAuthService;
import com.project.ccps.utils.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/menuAuth")
public class MenuAuthController {
    private static final Logger logger = LoggerFactory.getLogger(DepManageController.class);

    @Autowired
    MenuAuthService menuAuthService;

    //菜单添加
    @PostMapping("/create")
    public JSONResult create(@RequestBody MenuAuth menuAuth){
        JSONResult jsonResult = new JSONResult();
        try{
            int value = menuAuthService.create(menuAuth);
            if(value == ResultEnum.MENUREPEAT.getCode()){
                jsonResult.setStatusCode( ResultEnum.MENUREPEAT.getCode());
                jsonResult.setMessage( ResultEnum.MENUREPEAT.getMsg());
                return jsonResult;
            }
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //菜单查询
    @GetMapping("/selectAll")
    public JSONResult selectAll(String roleId){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData( menuAuthService.selectAll(roleId));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //编辑功能
    @PostMapping("/edit")
    public JSONResult edit(@RequestBody MenuAuth menuAuth){
        JSONResult jsonResult = new JSONResult();
        try {
            int value = menuAuthService.edit(menuAuth);
            if(value == ResultEnum.MENUREPEAT.getCode()){
                jsonResult.setStatusCode( ResultEnum.MENUREPEAT.getCode());
                jsonResult.setMessage( ResultEnum.MENUREPEAT.getMsg());
                return jsonResult;
            }
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //删除功能
    @GetMapping("/delete")
    public JSONResult delete(Integer id){
        JSONResult jsonResult = new JSONResult();
        try {
            int delValue  = menuAuthService.delete(id);
            if(delValue > 0){
                if(delValue == ResultEnum.MENUSUB.getCode()){
                    jsonResult.setStatusCode( ResultEnum.MENUSUB.getCode() );
                    jsonResult.setMessage( ResultEnum.MENUSUB.getMsg() );
                    return jsonResult;
                }
                jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
                jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            }
            return jsonResult;
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
}

package com.project.ccps.controller;

import com.project.ccps.dto.RoleAuthDto;
import com.project.ccps.model.Page;
import com.project.ccps.model.Role;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.RoleAuthService;
import com.project.ccps.utils.JSONResult;
import com.project.ccps.utils.OracleConn;
import com.project.ccps.utils.PageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/roleAuth")
public class RoleAuthController {
    private static final Logger logger = LoggerFactory.getLogger(DepManageController.class);

    @Value("${file.RETURN_URL}")
    private  String RETURN_URL;
    @Value("${file.ORACLE_USER}")
    private  String user;
    @Value("${file.ORACLE_PASSWORD}")
    private  String password;
    @Value("${file.ORACLE_JDBC}")
    private  String jdbc;
    @Autowired
    RoleAuthService roleAuthService;
    @Autowired
    RedisTemplate redisTemplate;
    /*
     * @author panxiaoliang
     * @details 获取推送的用户基本信息
     * @date 20201119
     * */
    @PostMapping("/acceptUserMsg")
    @CrossOrigin
    public JSONResult  acceptUserMsg(@RequestBody Map<String,Object> userMsg){
        JSONResult jsonResult = new JSONResult();
        try {
            String userAccount = (String)userMsg.get( "userAccount" );
            redisTemplate.opsForValue().set( userAccount,userMsg);
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
        jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        jsonResult.setData(RETURN_URL);
        return jsonResult;
    }

    /*
    * @author panxiaoliang
    * @details 根据账号获取用户是否有管理后台的权限，有显示进入后台管理，无界面不显示
    * @date 20201204
    * */
    @GetMapping("/backstageAuth")
    public int backstageAuth(String account){
        return OracleConn.backstageAuth(account,user,password,jdbc);
    }
    /*
     * @author panxiaoliang
     * @details 后台系统获用户信息数据
     * @date 20201119
     * */
    @GetMapping("/getUserMsg")
    public Map<String,Object> getUserMsg(String account){
        Map<String,Object> result = new HashMap<>(  );
        if(account != null && account != ""){
            result =  (Map<String,Object>) redisTemplate.opsForValue().get( account );
        }
        return result;
    }

    /*
     * @auhtor panxiaoliang
     * @Details 获取用户角色信息
     * @date 20201110
     * */
    @PostMapping("/getRoleList")
    public JSONResult getRoleList(@RequestBody Page page){
        JSONResult jsonResult = new JSONResult();
        try{
            List<Role> roleList = OracleConn.getRole(user,password,jdbc);
            Map<String,Object> map = PageUtil.startPage( roleList,page.getPage(),page.getSize() );
            List<Role> roleAuthList = (   List<Role> ) map.get("items");
            List<Role> roleList1 = new ArrayList<>(  );
            for(Role role:roleAuthList){
                roleList1.add(  roleAuthService.authMap(role)  );
            }
            map.put( "items",roleList1 );
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            jsonResult.setData( map );
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @Author：panxiaoliang
     * @Details:用户菜单，操作，栏目权限设置
     * @date 20201111
     * */
    @PostMapping("/rollAddAuth")
    public JSONResult rollAddAuth(@RequestBody RoleAuthDto roleAuthDto){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            jsonResult.setData(  roleAuthService.rollAddAuth(roleAuthDto) );
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @Author：panxiaoliang
     * @Details:用户操作权限
     * @date 20201112
     * */
    @GetMapping("/rollOperateAuth")
    public JSONResult rollOperateAuth(String roleId){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            if(roleId != null && roleId != ""){
                jsonResult.setData(  roleAuthService.rollOperateAuth(roleId) );
            }
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @auhtor panxiaoliang
     * @Details 根据用户获取对应的操作权限和菜单权限
     * @date 20201111
     * */
    @GetMapping("/getUserAuth")
    public JSONResult getUserAuth(int roleId){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            jsonResult.setData(  roleAuthService.getUserAuth(roleId) );
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @datails 获取所有角色信息
     * @date 20201116
     * */
    @GetMapping("/queryAllRole")
    public JSONResult queryAllRole(){
        JSONResult jsonResult = new JSONResult();
        try{
            List<Role> roleList = OracleConn.getRole(user,password,jdbc);
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            jsonResult.setData(  roleList );
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @Details 文章查看配置
     * @date 20201119
     * */
    @GetMapping("/articleLookAuth")
    public JSONResult articleLookAuth(@RequestParam("articleLookStr") String articleLookStr,@RequestParam("roleId") int roleId){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            jsonResult.setData(  roleAuthService.articleLookAuth(articleLookStr,roleId) );
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
}

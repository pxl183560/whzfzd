package com.project.ccps.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
//import com.project.ccps.annotation.UserLoginToken;
//import com.project.ccps.utils.AliyunOSSUtil;
//import com.project.ccps.utils.DeleteFileUtil;
//import com.project.ccps.utils.IntervalUtil;
//import com.project.ccps.utils.Response;
//import org.slf4j.LoggerFactory;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//import javax.annotation.Resource;
//import javax.imageio.ImageIO;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;

@RestController
@RequestMapping("/Aliyun")
public class AliyunImageUpload {

//    private final org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());
//    @Resource
//    private AliyunOSSUtil aliyunOSSUtil;
//
//    //多图片上传
//   @UserLoginToken
//    @PostMapping(value = "/uploadFile")
//    public Response uploadBlog(@RequestParam("files") MultipartFile[] file) throws IOException {
//        StringBuffer ss = new StringBuffer();
//        for (MultipartFile file1:file){
//            String filename = file1.getOriginalFilename();
//            try {
//                if (file1!=null) {
//                    if (!"".equals(filename.trim())) {
//                        File newFile = new File(filename);
//                        FileOutputStream os = new FileOutputStream(newFile);
//                        os.write(file1.getBytes());
//                        os.close();
//                        file1.transferTo(newFile);
//                        // 上传到OSS
//                        String uploadUrl = aliyunOSSUtil.upLoad(newFile);
//                        if (uploadUrl==null){
//                            return new Response("上传失败",401,null);
//                        }
//                        ss.append(uploadUrl+",");
//                        File file2 = new File("");
//                        String s = file2.getAbsolutePath();
//                        DeleteFileUtil.delete(s + File.separator + filename);
//                    }
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return new Response("上传成功",200,ss);
//    }
//
//    //带宽高的图片上传
//    @UserLoginToken
//    @PostMapping(value = "/uploadFileImage")
//    public Response uploadBlog1(@RequestParam("files") MultipartFile[] file,
//                               @RequestParam("width")String width,
//                               @RequestParam("height")String  height) throws IOException {
//        BufferedImage image = ImageIO.read(file[0].getInputStream());
//        double num=0.05;
//        List<String> widthList= Arrays.asList(width.split(","));
//        List<String> heightList= Arrays.asList(height.split(","));
//        String sss="";
//        String ssss="";
//        boolean inTheInterval=false;
//        boolean inTheInterval1=false;
//        for(int i=0;i<widthList.size();i++){
//            for(int j=0;j<heightList.size();j++){
//                sss=("(" + (Integer.parseInt(widthList.get(i))-Integer.parseInt(widthList.get(i))*num) +","+(Integer.parseInt(widthList.get(i))+Integer.parseInt(widthList.get(i))*num)+"]")  ;
//                ssss=("(" + (Integer.parseInt(heightList.get(i))-Integer.parseInt(heightList.get(i))*num) +","+(Integer.parseInt(heightList.get(i))+Integer.parseInt(heightList.get(i))*num)+"]");
//                inTheInterval = IntervalUtil.isInTheInterval(String.valueOf(image.getWidth()), sss);
//                inTheInterval1 = IntervalUtil.isInTheInterval(String.valueOf(image.getHeight()), ssss);
//                if((!inTheInterval || !inTheInterval1)){
//                    if(i!=widthList.size()){
//                        continue;
//                    }else {
//                        return new Response("上传失败",401,"上传文件和规定图片大小不匹配");
//                    }
//                }else {
//                    i=widthList.size();
//                    j=heightList.size();
//                }
//            }
//        }
//
//        StringBuffer ss = new StringBuffer();
//        for (MultipartFile file1:file){
//            String filename = file1.getOriginalFilename();
//            try {
//                if (file1!=null) {
//                    if (!"".equals(filename.trim())) {
//                        File newFile = new File(filename);
//                        FileOutputStream os = new FileOutputStream(newFile);
//                        os.write(file1.getBytes());
//                        os.close();
//                        file1.transferTo(newFile);
//                        // 上传到OSS
//                        String uploadUrl = aliyunOSSUtil.upLoad(newFile);
//                        if (uploadUrl==null){
//                            return new Response("上传失败",401,null);
//                        }
//                        ss.append(uploadUrl+",");
//                        File file2 = new File("");
//                        String s = file2.getAbsolutePath();
//                        DeleteFileUtil.delete(s + File.separator + filename);
//                    }
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return new Response("上传成功",200,ss);
//    }
//
//
//    @UserLoginToken
//    @RequestMapping(value = "/multiUpload")
//    public Response multiUpload(@RequestParam("video") MultipartFile[] multipartFile) throws IOException {
//        List<String> pathList=new ArrayList<>();
//        //保存文件的目录
//        String savePath="/root/upload/";
//        if(null!=multipartFile && multipartFile.length>0){
//            //遍历并保存文件
//            for(MultipartFile file:multipartFile){
//                pathList.add(savePath+file.getOriginalFilename());
//                file.transferTo(new File(savePath+file.getOriginalFilename()));
//            }
//        }
//        return new Response("上传成功",200,pathList);
//    }
}

package com.project.ccps.controller;

import com.project.ccps.annotation.Log;
import com.project.ccps.model.Theme;
import com.project.ccps.remsg.LogOperate;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.remsg.ResultValue;
import com.project.ccps.service.ThemeService;
import com.project.ccps.utils.JSONResult;
import com.project.ccps.utils.OracleConn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/theme")
public class ThemeController {
    private static final Logger logger = LoggerFactory.getLogger(DepManageController.class);
    @Value("${file.ORACLE_USER}")
    private  String user;
    @Value("${file.ORACLE_PASSWORD}")
    private  String password;
    @Value("${file.ORACLE_JDBC}")
    private  String jdbc;

    @Autowired
    ThemeService themeService;

    //主题添加
    @Log(LogOperate.THEME_ADD)
    @PostMapping("/create")
    public JSONResult create(@RequestBody Theme theme){
        JSONResult jsonResult = new JSONResult();
        try{
            return  ResultValue.methodResult( themeService.create(theme));
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //获取用户信息
    @GetMapping("/getUser")
    public JSONResult getUser(){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData( OracleConn.getUser(user,password,jdbc));
            jsonResult.setStatusCode(ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            return  jsonResult;
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //数据一览
    @GetMapping("/selectAll")
    public JSONResult selectAll(String roleId){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData( themeService.selectAll(roleId));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
    * @author panxiaoliang
    * @details 根据id获取栏目信息
    * @date 20201127
    * */
    @GetMapping("/selectByThemeId")
    public JSONResult selectByThemeId(int themeId){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData( themeService.selectByThemeId(themeId));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //获取网站的栏目
    @GetMapping("/selectSiteTheme")
    public JSONResult selectSiteTheme(){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData( themeService.selectSiteTheme());
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //编辑功能
    @Log(LogOperate.THEME_EDIT)
    @PostMapping("/edit")
    public JSONResult edit(@RequestBody Theme theme){
        JSONResult jsonResult = new JSONResult();
        try {
            return  ResultValue.methodResult( themeService.edit(theme));
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //删除功能
    @Log(LogOperate.THEME_DELETE)
    @PostMapping("/delete")
    public JSONResult delete(@RequestBody Map<String,Object> map){
        Integer id = (Integer)map.get( "id" );
        JSONResult jsonResult = new JSONResult();
        try {
            return  ResultValue.methodResult( themeService.delete(id));
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode(ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
}

package com.project.ccps.controller;

import com.project.ccps.annotation.Log;
import com.project.ccps.model.Article;
import com.project.ccps.model.Comment;
import com.project.ccps.model.Reply;
import com.project.ccps.remsg.LogOperate;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.remsg.ResultValue;
import com.project.ccps.service.ArticleService;
import com.project.ccps.utils.JSONResult;
import com.project.ccps.vo.ArticleVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/article")
public class ArticleController {
    private static final Logger logger = LoggerFactory.getLogger(DepManageController.class);

    @Autowired
    ArticleService articleService;

    /*
     * @author:panxiaoliang
     * @detail:文章创建
     * @date  20201118
     * */
    @Log(LogOperate.ARTICLE_ADD)
    @PostMapping("/create")
    public JSONResult create(@RequestBody Article article){
        JSONResult jsonResult = new JSONResult();
        if(article.getThemeId() == null ){
            jsonResult.setMessage( ResultEnum.CHOOSECOLUMN.getMsg() );
            jsonResult.setStatusCode( ResultEnum.CHOOSECOLUMN.getCode() );
            return jsonResult;
        }
        try {
            return  ResultValue.methodResult(articleService.create(article));
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 文章编辑
     * @date  20201118
     * */
    @Log(LogOperate.ARTICLE_EDIT)
    @PostMapping("/edit")
    public JSONResult edit(@RequestBody Article article){
        JSONResult jsonResult = new JSONResult();
        try {
            return  ResultValue.methodResult(articleService.edit(article));
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 查询文章
     * @date  20201118
     * */
    @PostMapping("/selectAll")
    public JSONResult selectAll(@RequestBody ArticleVo articleVo){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData(articleService.selectAll(articleVo));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 根据栏目名称获取文章信息
     * @date20201127
     * */
    @PostMapping("/selectArticleByThemeId")
    public JSONResult selectArticleByTheme(@RequestBody ArticleVo articleVo){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData(articleService.selectArticleByThemeId(articleVo));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode() );
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 查询待审核文章
     * @date  20201118
     * */
    @PostMapping("/checkArticle")
    public JSONResult checkArticle(@RequestBody ArticleVo articleVo){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData(articleService.checkArticle(articleVo));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     *@author panxiaoliang
     *@Detail:文章删除
     * */
    @Log(LogOperate.ARTICLE_DELETE)
    @PostMapping("/deletes")
    public JSONResult deletes(@RequestBody Map<String,String> map){
        JSONResult jsonResult = new JSONResult();
        try {
            String ids = map.get( "ids" );
            return  ResultValue.methodResult(articleService.deletes(ids));
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 置顶
     * */
    @Log(LogOperate.ARTICLE_PRIORITY)
    @PostMapping("/top")
    public JSONResult top ( @RequestBody Map<String,Object> map){
        JSONResult jsonResult = new JSONResult();
        Integer id = (Integer)map.get( "id" );
        Integer themeId = (Integer)map.get( "themeId" );
        try {
            articleService.top(id,themeId);
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 撤回
     * @date 20201215
     * */
    @Log(LogOperate.ARTICLE_REVOCATION)
    @PostMapping("/revocation")
    public JSONResult revocation(@RequestBody Map<String,Object> map){
        JSONResult jsonResult = new JSONResult();
        Integer id = (Integer)map.get( "id" );
        try {
            articleService.revocation(id);
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 优先级设置
     * */
    @Log(LogOperate.ARTICLE_PRIORITY)
    @PostMapping("/priority")
    public JSONResult priority( @RequestBody Map<String,String> map){
        JSONResult jsonResult = new JSONResult();
        String idStr = map.get( "idStr" );
        try {
            articleService.priority(idStr);
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /**
     * @author panxiaoliang
     * @detail 批量转稿  0 复制 1 剪贴   转稿类型
     * @return
     */
    //@Log(LogOperate.ARTICLE_TRANS)
    @PostMapping("/transArticle")
    public JSONResult transArticle( @RequestBody Map<String,Object> map){
        JSONResult jsonResult = new JSONResult();
        String articleIds = (String) map.get( "articleIds" );
        String themeId = (String) map.get( "themeId" ) ;
        String transType = (String)map.get( "transType" );
        try{
            articleService.transArticle(articleIds,themeId,Integer.parseInt( transType ));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * 转稿文章列表
     * */
    @PostMapping("/selectTransAll")
    public JSONResult selectTransAll(@RequestBody ArticleVo articleVo){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData(articleService.selectTransAll(articleVo));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    //查询单个文章
    @GetMapping("/selectById")
    public JSONResult selectById(@RequestParam("id") Integer id,@RequestParam(value = "jf",defaultValue = "0")Integer jf){//flag 0简体 1繁体
        JSONResult jsonResult = new JSONResult();
        Map<String,String> selectValue  = articleService.selectById(id,jf);
        jsonResult.setData(selectValue);
        return jsonResult;
    }

    /*
     * 撤销稿件
     * */
    @Log(LogOperate.ARTICLE_BACKOUT)
    @PostMapping("/backout")
    public JSONResult backout(@RequestBody Map<String,Object> map){
        Integer  articleId = (Integer)map.get( "articleId" );
        Integer checkStatus = (Integer)map.get("checkStatus");
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.backout(articleId,checkStatus));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * 回退稿件
     * */
    @Log(LogOperate.ARTICLE_ROLLBACK)
    @PostMapping("/rollBack")
    public JSONResult rollBack (@RequestBody Map<String,Object> map){
        Integer articleId = (Integer)map.get( "articleId" );
        String backReason = (String) map.get( "backReason" );
        String checkUserName = (String)map.get( "checkUserName" );
        String checkUserId = String.valueOf( map.get( "checkUserId" ) ) ;
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.rollBack(articleId,backReason,checkUserName,Integer.parseInt( checkUserId )));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * 发布稿件
     * */
    @Log(LogOperate.ARTICLE_ROLLBACK)
    @PostMapping("/sendArticle")
    public JSONResult sendArticle (@RequestBody Map<String,Object> map){
        Integer articleId = (Integer)map.get( "articleId" );
        String checkUserName = (String)map.get( "checkUserName" );
        Integer checkUserId = (Integer)map.get( "checkUserId" );
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.sendArticle(articleId,checkUserName,checkUserId));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @details 根据文章id 获取对应文章信息
     * @date 20201127
     *
     * */
    @GetMapping("/getArticleById")
    public JSONResult getArticleById(int id){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.getArticleById(id));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @details 根据文章id 获取对应文章信息
     * @date 20201127
     * */
    @GetMapping("/getNewArticle")
    public JSONResult getNewArticle(){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.getNewArticle());
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @details 替换文章文件路径
     * @date 2201130
     * */
    @GetMapping("/replaceUrl")
    public JSONResult replaceUrl(){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.replaceUrl());
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @details 文章评论添加
     * @date 20201211
     * */
    @PostMapping("/comment")
    public JSONResult comment(@RequestBody Comment comment){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.comment(comment));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @details 评论回复添加
     * @date 20201211
     * */
    @PostMapping("/reply")
    public JSONResult comment(@RequestBody Reply reply){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.reply(reply));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @details 评论回复添加
     * @date 20201211
     *
     * */
    @GetMapping("/selectComment")
    public JSONResult selectComment(Integer id){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.selectComment(id));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 部门统计
     * */
    @PostMapping("/depArticleStatistics")
    public JSONResult depArticleStatistics(@RequestBody  Map<String,Object> time){
        JSONResult jsonResult = new JSONResult();
        try{
            jsonResult.setData(articleService.depArticleStatistics(time));
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
}

package com.project.ccps.controller;

import com.project.ccps.dto.SysLogDto;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.SysLogService;
import com.project.ccps.utils.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sysLog")
public class SysLogController {
    private static final Logger logger = LoggerFactory.getLogger(DepManageController.class);

    @Autowired
    SysLogService sysLogService;

    @PostMapping("/selectAll")
    public JSONResult selectAll(@RequestBody SysLogDto sysLogDto){
        JSONResult jsonResult = new JSONResult();
        try {
            jsonResult.setData(sysLogService.selectAll(sysLogDto) );
            jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
            jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
            return jsonResult;
        }catch (Exception e){
            logger.error( e.getMessage() );
            jsonResult.setStatusCode( ResultEnum.RUNEXECEPTION.getCode());
            jsonResult.setMessage( ResultEnum.RUNEXECEPTION.getMsg());
        }
        return jsonResult;
    }
}

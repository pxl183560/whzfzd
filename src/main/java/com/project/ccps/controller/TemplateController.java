package com.project.ccps.controller;

import com.project.ccps.model.Page;
import com.project.ccps.model.Template;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.TemplateService;
import com.project.ccps.service.ThemeService;
import com.project.ccps.utils.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/template")
public class TemplateController {
    @Autowired
    TemplateService templateService;
    @Autowired
    ThemeService themeService;

    /*
    * @author panxiaoliang
    * @detail 查询所有模板
    * @date 20201123
    * */
    @PostMapping("/selectAll")
    public JSONResult selectAll(@RequestBody  Page page){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData( templateService.selectAll(page));
        jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
        jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        return jsonResult;
    }

    /*
    * @author panxiaoliang
    * @detail 模板配置
    * @date 20201123
    * */
    @PostMapping("/templateDeploy")
    public JSONResult templateDeploy(@RequestBody Template templete){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setData( templateService.templateDeploy(templete));
        jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
        jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        return jsonResult;
    }

    /*
    * @author panxiaoliang
    * @detail 页面信息数据获取
    * @date 20201123
    * */
    @PostMapping("/getPageData")
    public JSONResult getPageData(@RequestBody String pageName){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
        jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        return jsonResult;
    }

    /*
    * @author panxiaoliang
    * @detail 获取模板首页
    * @date 20201125
    * */
    @GetMapping("/template1")
    public JSONResult  template1(Integer tempId){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
        jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        jsonResult.setData(  templateService.template1(tempId));
        return jsonResult;
    }

    /*
     * @author panxiaoliang
     * @detail 统计网站历史访问量
     * @date 20201210
     * */
    @GetMapping("/websiteViewCount")
    public JSONResult  websiteViewCount(){
        JSONResult jsonResult = new JSONResult();
        jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
        jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        jsonResult.setData(  templateService.websiteViewCount());
        return jsonResult;
    }
}

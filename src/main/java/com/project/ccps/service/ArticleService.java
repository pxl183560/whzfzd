package com.project.ccps.service;

import com.project.ccps.dto.ArticleByIdDto;
import com.project.ccps.model.Article;
import com.project.ccps.model.Comment;
import com.project.ccps.model.Reply;
import com.project.ccps.vo.ArticleVo;

import java.util.List;
import java.util.Map;

public interface ArticleService {

    int create(Article article);

    Map<String,Object>  selectAll(ArticleVo articleVo);

    int deletes(String ids);

    Object selectColumn();

    int edit(Article article);

    Map<String,String> selectById(Integer id,Integer jf);

    Map<String,Object> selectImageSize(Integer columNumber, Integer position);

    List<Article> findGoodArticle(Integer jf);

    void priority(String idStr);

    void transArticle(String articleIds, String themeId, Integer transType);

    int backout(int articleId,int checkStatus);

    int rollBack(int articleId, String backReason,String checkUserName,Integer checkUserId);

    Map<String,Object>  checkArticle(ArticleVo articleVo);

    void top(Integer id,Integer themeId);

    int sendArticle(int articleId,String checkUserName,Integer checkUserId);

    Map<String,Object>  selectTransAll(ArticleVo articleVo);

    ArticleByIdDto getArticleById(int id);

    List<Map<String,String>> getNewArticle();

    Map<String,Object> selectArticleByThemeId(ArticleVo articleVo);

    Object replaceUrl();

    Object comment(Comment comment);

    Object reply(Reply reply);

    Object selectComment(Integer id);

    int revocation(Integer id);

    List<Map<String,Object>> depArticleStatistics(Map<String,Object> time);
}

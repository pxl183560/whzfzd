package com.project.ccps.service;


import com.project.ccps.model.Common;

public interface PageService {
    Object template1(Integer columnNumber);

    Object template2(Integer columnNumber);

    Object template3(Integer columnNumber);

    Object template4(Integer columnNumber);

    Object template5(Integer columnNumber);

    Object template6(Integer columnNumber);

    Object selectValue(Common common);

    Object jumpArticle(Integer secondThemeId, Integer articleId);

    Object jumpSecondColumn(Integer secondThemeId, Integer articleId);
}

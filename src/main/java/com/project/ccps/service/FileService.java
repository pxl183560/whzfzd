package com.project.ccps.service;

import java.util.List;

public interface FileService {

    String selectFilePath(String id);

    void delFile(String id);

    long articleMaxTime();

    List<String> articleFileReqUrl();
}

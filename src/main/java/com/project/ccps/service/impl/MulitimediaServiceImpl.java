package com.project.ccps.service.impl;

import com.project.ccps.model.FileEntity;
import com.project.ccps.service.MulitimediaService;
import com.project.ccps.utils.*;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class MulitimediaServiceImpl implements MulitimediaService {
    @Resource
    private AliyunOSSUtil aliyunOSSUtil;
    @Override
    public JSONResult upload(MultipartFile multipartFile, HttpServletRequest request) {
        JSONResult result = new JSONResult();
        FileEntity entity = new FileEntity();
        FileUploadTool fileUploadTool = new FileUploadTool();
        try {
            entity = fileUploadTool.createFile(multipartFile, request);
            if (entity != null) {
                result.setStatusCode(0);
                result.setMessage("上传成功");
            } else {
                result.setStatusCode(0);
                result.setMessage("上传失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int uploadFilesOriginal(List<MultipartFile> files) throws IOException {
        String path = "g:\\image\\";
        int backValue = 0;
        if (files.size() < 1) {
            backValue = 1;
            return backValue;
        }
        for (int i = 0; i < files.size(); ++i) {
            MultipartFile file = files.get(i);
            if (!file.isEmpty()) {
                String filename = file.getOriginalFilename();
                String type = filename.indexOf(".") != -1
                        ? filename.substring(filename.lastIndexOf("."), filename.length())
                        : null;
                if (type == null) {
                    backValue = 1;
                    return backValue;
                }

                if (!(".PNG".equals(type.toUpperCase()) || ".JPG".equals(type.toUpperCase()))) {
                    backValue = 1;
                    return backValue;
                }
            }
        }
        for (int i = 0; i < files.size(); ++i) {
            MultipartFile file = files.get(i);
            String filename = file.getOriginalFilename();
            String type = filename.indexOf(".") != -1 ? filename.substring(filename.lastIndexOf("."), filename.length())
                    : null;
            String filepath = path + filename;
            File filesPath = new File(path);
            if (!filesPath.exists()) {
                filesPath.mkdir();
            }
            BufferedOutputStream out = null;
            type = filepath.indexOf(".") != -1 ? filepath.substring(filepath.lastIndexOf(".") + 1, filepath.length())
                    : null;
            try {
                out = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                out.write(file.getBytes());
            } catch (Exception e) {
                // 没有上传成功
                backValue = 1;
                return backValue;
            } finally {
                out.flush();
                out.close();
            }
        }
        return backValue;
    }

    //阿里云文件上传
    public Response aliyunImageUpload(MultipartFile[] file) {
        StringBuffer ss = new StringBuffer();
        for (MultipartFile file1:file){
            String filename = file1.getOriginalFilename();
            try {
                if (file1!=null) {
                    if (!"".equals(filename.trim())) {
                        File newFile = new File(filename);
                        FileOutputStream os = new FileOutputStream(newFile);
                        os.write(file1.getBytes());
                        os.close();
                        file1.transferTo(newFile);
                        // 上传到OSS
                        String uploadUrl = aliyunOSSUtil.upLoad(newFile);
                        if (uploadUrl==null){
                            return new Response("上传失败",401,null);
                        }
                        ss.append(uploadUrl+",");
                        File file2 = new File("");
                        String s = file2.getAbsolutePath();
                        DeleteFileUtil.delete(s + File.separator + filename);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return new Response("上传成功",200,ss);
    }
}

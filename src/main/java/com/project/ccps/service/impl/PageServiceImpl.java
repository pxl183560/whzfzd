package com.project.ccps.service.impl;

import com.project.ccps.mapper.PageMapper;
import com.project.ccps.model.Common;
import com.project.ccps.service.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PageServiceImpl implements PageService {
    @Autowired
    PageMapper firstPageMapper;
    @Override
    public Map<Integer,Object> template1(Integer columnNumber) {
        List<Map<String,String>> template = firstPageMapper.selectTemplate(columnNumber);
        Map<Integer,Object> value = new HashMap<>();
        for(int index = 0; index <= template.size();index++){
            //多图多文章添加
            if(index == 0 || index == 1 || index == 3 || index == 4 || index == 5 || index == 6){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }
            //
            if(index == 2){
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                value.put(index,article);
            }
            if(index == 7){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                List<Map<String,String>>  article =  firstPageMapper.selectDesc(Integer.parseInt(secondThemeId),articleImage.size());
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }
        }
        return value;
    }

    @Override
    public Object template2(Integer columnNumber) {
        List<Map<String,String>> template = firstPageMapper.selectTemplate(columnNumber);
        Map<Integer,Object> value = new HashMap<>();
        for(int index = 0; index <= template.size();index++){
            if(index == 0 || index == 1 || index == 2 || index == 5 || index == 6){
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                value.put(index,article);
            }
            if(index == 3){
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage8(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }
            if(index == 4){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =  firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }

            if(index == 9 || index == 7){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage3(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }
            if(index == 8){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImageSigle(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }
        }
        return value;
    }

    @Override
    public Object template3(Integer columnNumber) {
        List<Map<String,String>> template = firstPageMapper.selectTemplate(columnNumber);
        Map<Integer,Object> value = new HashMap<>();
        for(int index = 0; index <= template.size();index++){
            if(index == 0 || index == 1){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }
            if(index == 2 || index == 3){
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }
            if(index == 4){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }
        }
        return value;
    }

    @Override
    public Object template4(Integer columnNumber) {
        List<Map<String,String>> template = firstPageMapper.selectTemplate(columnNumber);
        Map<Integer,Object> value = new HashMap<>();
        for(int index = 0; index <= template.size();index++){
            if(index == 0 || index == 1){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }
            if(index == 2 || index == 3){
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =  firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List list1 = article.subList(0, 7);
                List list2 = article.subList(7, 14);
                value.put(index,list1);
                value.put(index+3,list2);
            }
            if(index == 4){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }
        }
        return value;
    }

    @Override
    public Object template5(Integer columnNumber) {
        List<Map<String,String>> template = firstPageMapper.selectTemplate(columnNumber);
        Map<Integer,Object> value = new HashMap<>();
        for(int index = 0; index <= template.size();index++){
            if(index == 0 || index == 1 || index == 2){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }
            if(index == 3){
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =  firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                value.put(index,article);
            }
            if(index == 4){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }

        }
        return value;
    }

    @Override
    public Object template6(Integer columnNumber) {
        List<Map<String,String>> template = firstPageMapper.selectTemplate(columnNumber);
        Map<Integer,Object> value = new HashMap<>();
        for(int index = 0; index <= template.size();index++){
            if(index == 0){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }

            if(index == 1){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =   firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImageSigle(Integer.parseInt(secondThemeId));
                obj.add(article);
                obj.add(articleImage);
                value.put(index,obj);
            }

            if(index == 2 || index == 3 || index == 4  || index == 5 || index == 6){
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                String showNumber = String.valueOf(template.get(index).get("showNumber"));
                List<Map<String,String>>  article =  firstPageMapper.selectArticle(Integer.parseInt(secondThemeId),Integer.parseInt(showNumber));
                value.put(index,article);
            }
            if(index == 7){
                List<Object> obj = new ArrayList<>();
                String  secondThemeId = String.valueOf(template.get(index).get("secondThemeId"));
                List<Map<String,String>>  articleImage =  firstPageMapper.selectArticleImage(Integer.parseInt(secondThemeId));
                value.put(index,articleImage);
            }
        }
        return value;
    }

    @Override
    public Object selectValue(Common common) {
        if(common.getPageNumber() == null){
            common.setPageNumber(1);
        }
        Map<String,Object> result = new HashMap<>();
        int size = 10;
        int total = firstPageMapper.selectValueCount(common);
        int pageSize = (total/size);
        int remainder = total%size;
        if(remainder > 0 && remainder < size){
            pageSize = (total/size) + 1;
        }
        int startPage = (common.getPageNumber() - 1) * size;
        int endPage = size;
        common.setStartPage(startPage);
        common.setEndPage(endPage);
        result.put("total",total);//总叶数
        result.put("pageSize",pageSize);//分页数
        result.put("page",common.getPageNumber());//页码
        result.put("size",size);//每页展示的条数
        result.put("items", firstPageMapper.selectValue(common));
        return result;
    }


    @Override
    public Object jumpSecondColumn(Integer firstThemeId, Integer secondThemeId) {
        Map<String,Object>  result = new HashMap<>();
        Map<String,String> firstTheme = firstPageMapper.firstThemeName(firstThemeId);
        List<Map<String,String>> secondName = firstPageMapper.secondName(firstThemeId);
        List<Map<String,String>>  articleTitle  = firstPageMapper.articleTitle(secondThemeId);
        result.put("firstTheme",firstTheme);
        result.put("secondName",secondName);
        result.put("articleTitle",articleTitle);
        return result;
    }

    @Override
    public Object jumpArticle(Integer secondThemeId, Integer articleId) {
        Map<String,Object>  result = new HashMap<>();
        Map<String,String> secondThemeName = firstPageMapper.secondThemeName(secondThemeId);
        List<Map<String,String>> articleTitle = firstPageMapper.articleTitle(secondThemeId);
        String  articleContext  = firstPageMapper.articleContext(articleId);
        result.put("secondTheme",secondThemeName);
        result.put("articleTitle",articleTitle);
        result.put("articleContext",articleContext);
        return result;
    }
}

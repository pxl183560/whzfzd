package com.project.ccps.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.project.ccps.dto.ThemeDto;
import com.project.ccps.mapper.ThemeMapper;
import com.project.ccps.model.Theme;
import com.project.ccps.model.User;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.ThemeService;
import com.project.ccps.utils.ListToTree;
import com.project.ccps.utils.OracleConn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ThemeServiceImpl implements ThemeService {

    @Value("${file.ORACLE_USER}")
    private  String user;
    @Value("${file.ORACLE_PASSWORD}")
    private  String password;
    @Value("${file.ORACLE_JDBC}")
    private  String jdbc;

    @Autowired
    ThemeMapper themeMapper;

    @Override
    @Transactional
    public int  create(Theme theme) {
        if(theme.getParentId() != 0){
            int ifcheck = themeMapper.queryIfCheck(theme.getParentId());
            if(ifcheck == 1){
                return ResultEnum.NEEDCHECK.getCode();
            }
            int articleValue = themeMapper.queryArticleCount( theme.getParentId() );
            if(articleValue > 0){
                return ResultEnum.ARTICENODE.getCode();
            }
        }
        int value = themeMapper.queryByThemeName(theme.getName());
        if(value > 0){
            return ResultEnum.COLUMNREPEAT.getCode();
        }
        //获取用户名称
        theme = getUserName(theme);
        themeMapper.create(theme);
        int themeId = theme.getId();
        insertThemeUser(theme.getCheckUserId(),theme.getCheckUserName(),themeId);
        return ResultEnum.SUCCESS.getCode();
    }

    @Override
    @Transactional
    public int edit(Theme theme) {
        int value = themeMapper.queryThemeCount(theme.getName(),theme.getId());
        if(value > 0){
            return ResultEnum.COLUMNREPEAT.getCode();
        }
        theme = getUserName(theme);//获取用户名称
        if(theme.getIfCheck() == 0){
            themeMapper.delByTheme(theme.getId());
        }
        if(theme.getIfCheck() != 0){
            int subCount = themeMapper.selectSub(theme.getId());
            if(subCount > 0){
                return  ResultEnum.EXITSUB.getCode();
            }
        }
        themeMapper.edit(theme);
        if(theme.getCheckUserId().size() > 0 && theme.getIfCheck() != 0){
            themeMapper.delByTheme(theme.getId());
            insertThemeUser(theme.getCheckUserId(),theme.getCheckUserName(),theme.getId());
        }
        return  ResultEnum.SUCCESS.getCode();
    }

    //获取用户名称
    public Theme getUserName(Theme theme){
        List<User> userList = OracleConn.getUser(user,password,jdbc);
        List<String> checkUserNamelist = new ArrayList<>(  );
        for(User user:userList){
            for(Integer checkUserId:theme.getCheckUserId() ){
                if(checkUserId.equals( user.getId() )){
                    checkUserNamelist.add( user.getUserName() );
                }
            }
        }
        theme.setCheckUserName( checkUserNamelist );
        return theme;
    }


    public  void insertThemeUser(List<Integer> checkUserId,List<String> checkUserName,int themeId){
        for(int i = 0;i<checkUserId.size();i++){
            themeMapper.insertThemeUser(themeId,checkUserId.get( i ),checkUserName.get( i ));
        }
    }

    @Override
    public int delete(Integer id) {
        int nodeCount = themeMapper.queryNode(id);//查询是否存在子集栏目节点。
        if(nodeCount > 0){
            return ResultEnum.COLUMNSUB.getCode();
        }
        int articleCount = themeMapper.queryArticleCount(id);
        if(articleCount > 0){
            return ResultEnum.ARTICLESUB.getCode();
        }
        themeMapper.delete(id);
        themeMapper.delByTheme(id);
        return ResultEnum.SUCCESS.getCode();
    }

    @Override
    public JSONArray selectAll(String roleId) {
        String sql = "where id in(select themeId from role_theme_auth where roleId in ("+roleId+"))";
        if(roleId == null || roleId == ""){
            sql = "";
        }
        List<Theme> themeList = themeMapper.selectAll(sql);
        for(Theme theme:themeList){
            List<Integer> checkUserId = themeMapper.queyUserByThemeId(theme.getId());
            String checkUserName = themeMapper.checkUserName(theme.getId());
            if(checkUserName != null){
                theme.setCheckUserStr( checkUserName );
            }
            if(checkUserId != null){
                theme.setCheckUserId( checkUserId );
            }
        }
        JSONArray result = ListToTree.listToTree(JSONArray.parseArray( JSON.toJSONString(themeList)),"id","parentId","children");
        return result;
    }

    @Override
    public  JSONArray selectSiteTheme() {
        List<ThemeDto> themeDtoList = themeMapper.selectSiteTheme();
        JSONArray result = ListToTree.listToTree(JSONArray.parseArray( JSON.toJSONString(themeDtoList)),"id","parentId","children");
        return result;
    }

    @Override
    public JSONArray selectByThemeId(int themeId) {
        List<Theme> themeList = themeMapper.selectByThemeId(themeId);
        JSONArray result = ListToTree.listToTree(JSONArray.parseArray( JSON.toJSONString(themeList)),"id","parentId","children");
        return result;
    }
}

package com.project.ccps.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.project.ccps.mapper.MenuAuthMapper;
import com.project.ccps.model.MenuAuth;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.MenuAuthService;
import com.project.ccps.utils.ListToTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MenuAuthServiceImpl implements MenuAuthService {

    @Autowired
    MenuAuthMapper menuAuthMapper;

    @Override
    @Transactional
    public int  create(MenuAuth menuAuth) {
        int value = menuAuthMapper.queryByMenuName(menuAuth.getMenuName());
        if(value > 0){
            return ResultEnum.MENUREPEAT.getCode();
        }
        return menuAuthMapper.create(menuAuth);
    }

    @Override
    @Transactional
    public int edit(MenuAuth menuAuth) {
        int value = menuAuthMapper.queryMenuNameCount(menuAuth.getMenuName(),menuAuth.getId());
        if(value > 0){
            return ResultEnum.MENUREPEAT.getCode();
        }
        return menuAuthMapper.edit(menuAuth);
    }

    public void addMenuRole(Integer menuId,Integer[] roleArr){
        for(Integer roleId:roleArr){
            menuAuthMapper.insertMenuRole(menuId,roleId);
        }
    }

    @Override
    @Transactional
    public int delete(Integer id) {
        int nodeCount = menuAuthMapper.queryNode(id);//查询是否存在子集节点。
        if(nodeCount > 0){
            return ResultEnum.MENUSUB.getCode();
        }
        int delValue = menuAuthMapper.delete(id);
        if(delValue > 0){
            menuAuthMapper.deleteMenuRole( id );
        }
        return delValue;
    }

    @Override
    public JSONArray selectAll(String roleId) {
        String sql = " where id in (select menuId from role_menu_auth where roleId in ("+roleId+")) order by sortIndex ASC,updateTime ASC";
        if(roleId == null || "".equals( roleId )){
            sql = " order by sortIndex ASC,updateTime ASC";
        }
        List<MenuAuth> themeList = menuAuthMapper.selectAll(sql);
        JSONArray result = ListToTree.listToTree(JSONArray.parseArray( JSON.toJSONString(themeList)),"id","parentId","children");
        return result;
    }
}

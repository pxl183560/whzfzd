package com.project.ccps.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.houbb.opencc4j.util.ZhConverterUtil;
import com.project.ccps.dto.ArticleByIdDto;
import com.project.ccps.dto.ArticleDto;
import com.project.ccps.mapper.ArticleMapper;
import com.project.ccps.mapper.ThemeMapper;
import com.project.ccps.model.Article;
import com.project.ccps.model.Comment;
import com.project.ccps.model.Reply;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.ArticleService;
import com.project.ccps.utils.ListToTree;
import com.project.ccps.utils.UrlChange;
import com.project.ccps.vo.ArticleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    ArticleMapper articleMapper;
    @Autowired
    ThemeMapper themeMapper;

    @Override
    public int create(Article article) {
        int ifCheck = themeMapper.queryIfCheck( article.getThemeId() );
        //创建文章判断是否需要审核不需要审核直接发布（暂存除外）
        if(ifCheck == 0 && (article.getCheckStatus() == null || article.getCheckStatus() != 4)){
            article.setCheckStatus( 1 );
        }
        int value = articleMapper.selectByArticelName(article.getTitle(),article.getThemeId());
        int themeChildCount = articleMapper.themeChildCount(article.getThemeId());
        if(themeChildCount > 0){
            return ResultEnum.CHILDNODE.getCode();
        }
        if(value > 0 ){
            return  ResultEnum.ARTICLEREPEAT.getCode();
        }
        return articleMapper.create(article);
    }

    @Override
    public int edit(Article article) {
        int ifCheck = themeMapper.queryIfCheck( article.getThemeId() );
        //创建文章判断是否需要审核不需要审核直接发布（暂存除外）
        if(ifCheck == 0 && (article.getCheckStatus() == null || article.getCheckStatus() != 4)){
            article.setCheckStatus( 1 );
        }
        int value = articleMapper.selectCountByName(article);
        if(value > 0 ){
            return  ResultEnum.ARTICLEREPEAT.getCode();
        }
        return articleMapper.edit(article);
    }

    @Override
    public  Map<String,Object> selectAll(ArticleVo articleVo) {
        //设置管理员能看所有的文章
        if(articleVo.getRoleId().contains( "2133" )){
            articleVo.setFlag( 1 );
        }
        articleVo.setThemeIdStr( articleMapper.getThemeIdStr(articleVo.getThemeId()) );
        Map<String,Object> result = new HashMap<>();
        int size = articleVo.getSize();
        int total = articleMapper.selectAllCount(articleVo);
        int pageSize = (total/size);
        int remainder = total%size;
        if(remainder > 0 && remainder < size){
            pageSize = (total/size) + 1;
        }
        int startPage = (articleVo.getPage() - 1) * size;
        int endPage = size;
        List<ArticleDto> topArticle = new ArrayList<>(  );
        if(articleVo.getPage() == 1){
            topArticle  = articleMapper.topArticle(articleVo);
            articleVo.setTopArticleId( articleMapper.topArticleId(articleVo));
        }
        int topSize = topArticle.size();
        if(topSize> 0){
            startPage = startPage + topSize;
            endPage = endPage - topSize;
        }
        articleVo.setStartPage(startPage);
        articleVo.setEndPage(endPage);
        articleVo.setTimeParam( articleMapper.getTimeParam(articleVo) );
        List<Integer> idList = articleMapper.idList(articleVo);
        List<ArticleDto> articleDtos = new ArrayList<>(  );
        if(idList.size() != 0 ){
            articleDtos = articleMapper.selectAll(idList);
            topArticle.addAll( articleDtos );
            result.put("items",topArticle);
        } else{
            result.put("items",articleDtos);
        }
        result.put("total",total);//总叶数
        result.put("pageSize",pageSize);//分页数
        result.put("page",articleVo.getPage());//页码
        result.put("size",size);//每页展示的条数
        return result;
    }

    @Override
    public Map<String, Object> selectArticleByThemeId(ArticleVo articleVo) {
        Map<String,Object> result = new HashMap<>();
        articleVo.setThemeIdStr( articleMapper.getThemeIdStr( articleVo.getThemeId() ) );
        int size = articleVo.getSize();
        int total = articleMapper.selectArticleByThemeIdCount(articleVo);
        int pageSize = (total/size);
        int remainder = total%size;
        if(remainder > 0 && remainder < size){
            pageSize = (total/size) + 1;
        }
        int startPage = (articleVo.getPage() - 1) * size;
        int endPage = size;
        articleVo.setStartPage(startPage);
        articleVo.setEndPage(endPage);
        List<ArticleDto> articleDtos = articleMapper.selectArticleByThemeId(articleVo);
        result.put("total",total);//总叶数
        result.put("pageSize",pageSize);//分页数
        result.put("page",articleVo.getPage());//页码
        result.put("size",size);//每页展示的条数
        result.put("items",articleDtos);
        return result;
    }

    @Override
    @Transactional
    public Object replaceUrl() {
        //获取含有文件路径的所有文章
        List<Article> articleList = articleMapper.queryArticleContent();
        List<String> fileDirList = UrlChange.getFileList();
        String content = "";
        for(Article article:articleList){
            content = article.getContent();
            for(String dir:fileDirList){
                boolean status = content.contains(dir);
                if(status){
                    content = UrlChange.processImgSrc(content,dir);
                    articleMapper.updateContent(content,article.getId());
                }
            }
        }
        return ResultEnum.SUCCESS.getMsg();
    }

    @Override
    public Object comment(Comment comment) {
        return articleMapper.comment(comment);
    }

    @Override
    public Object reply(Reply reply) {
        return articleMapper.reply(reply);
    }

    @Override
    public Map<String, Object> selectTransAll(ArticleVo articleVo) {
        Map<String,Object> result = new HashMap<>();
        int size = articleVo.getSize();
        int total = articleMapper.selectTransAllCount(articleVo);
        int pageSize = (total/size);
        int remainder = total%size;
        if(remainder > 0 && remainder < size){
            pageSize = (total/size) + 1;
        }
        int startPage = (articleVo.getPage() - 1) * size;
        int endPage = size;
        articleVo.setStartPage(startPage);
        articleVo.setEndPage(endPage);
        List<ArticleDto> articleDtos = articleMapper.selectTransAll(articleVo);
        result.put("total",total);//总叶数
        result.put("pageSize",pageSize);//分页数
        result.put("page",articleVo.getPage());//页码
        result.put("size",size);//每页展示的条数
        result.put("items",articleDtos);
        return result;
    }

    @Override
    @Transactional
    public ArticleByIdDto getArticleById(int id) {
        articleMapper.readCount(id);
        ArticleByIdDto articleByIdDto = articleMapper.getArticleById(id);
        articleByIdDto.setFrontId( articleMapper.selectFrontId(id,articleByIdDto.getThemeId()) );
        articleByIdDto.setLaterId( articleMapper.selectLaterId(id,articleByIdDto.getThemeId()) );
        return articleByIdDto;
    }

    @Override
    public Object selectComment(Integer id) {
        List<Comment> commentList = articleMapper.selectComment(id);
        for(Comment comment:commentList){
            comment.setReplyList( articleMapper.selectReply(comment.getId()) );
        }
        return commentList;
    }

    @Override
    public int revocation(Integer id) {
        return articleMapper.revocation(id);
    }

    @Override
    public List<Map<String, Object>> depArticleStatistics(Map<String,Object> time) {
        String startTime = "";
        String endTime = "";
        String  themeIdStr = "";
        if( time.get( "startTime" ) != null && time.get( "startTime" ) != ""){
            startTime = (String)time.get( "startTime" );
        }
        if( time.get( "endTime" ) != null && time.get( "endTime" ) != ""){
            endTime = (String)time.get( "endTime" );
        }
        if( time.get( "themeId" ) != null && time.get( "themeId" ) != "" ){
            themeIdStr = articleMapper.getThemeIdStr( (Integer) time.get( "themeId" ) );
        }
        return articleMapper.depArticleStatistics(startTime,endTime,themeIdStr);
    }

    @Override
    public List<Map<String,String>> getNewArticle() {
        Integer id = articleMapper.getInteractionId();
        List<Map<String,String>> result = articleMapper.getNewArticle(id);
        return result;
    }

    @Override
    public Map<String, Object> checkArticle(ArticleVo articleVo) {
        if(articleVo.getRoleId().contains( "2133" ) ){
            articleVo.setFlag( 1 );
        }
        Map<String,Object> result = new HashMap<>();
        int size = articleVo.getSize();
        int total = articleMapper.checkArticleCount(articleVo);
        int pageSize = (total/size);
        int remainder = total%size;
        if(remainder > 0 && remainder < size){
            pageSize = (total/size) + 1;
        }
        int startPage = (articleVo.getPage() - 1) * size;
        int endPage = size;
        articleVo.setStartPage(startPage);
        articleVo.setEndPage(endPage);
        List<ArticleDto> articleDtos = articleMapper.checkArticle(articleVo);
        result.put("total",total);//总叶数
        result.put("pageSize",pageSize);//分页数
        result.put("page",articleVo.getPage());//页码
        result.put("size",size);//每页展示的条数
        result.put("items",articleDtos);
        return result;
    }

    @Override
    public int deletes(String ids) {
        if(ids == null && ids == ""){
            return ResultEnum.ARTICLEREPEAT.getCode();
        }
        return articleMapper.deletes(ids);
    }

    @Override
    @Transactional
    public void priority(String idStr) {
        articleMapper.updatePriority(); //将原来的优先级文章取消
        articleMapper.priority(idStr);//对文章进行重新置顶
    }

    @Override
    @Transactional
    public void top(Integer id,Integer themeId) {
        articleMapper.updateTop(themeId); //将原来的置顶文章取消
        articleMapper.top(id);//对文章进行重新置顶
    }

    @Override
    public int sendArticle(int articleId,String checkUserName,Integer checkUserId) {
        return articleMapper.sendArticle(articleId,checkUserName,checkUserId);
    }

    @Override
    @Transactional
    public void transArticle(String articleIds, String themeId, Integer transType) {
        //复制
        List<Article> articleList = articleMapper.selectArticleByIds(articleIds);
        if(themeId != null){
            String [] themeIdStr = themeId.split( "," );
            for(String str : themeIdStr ){
                for(Article article:articleList){
                    article.setThemeId( Integer.parseInt( str ) );
                }
                for(Article article : articleList){
                    articleMapper.create(article);
                }
            }
        }
        if(transType == 1){
            articleMapper.delByIds(articleIds);
        }
    }

    @Override
    public int backout(int articleId,int checkStatus) {
        return articleMapper.backout(articleId,checkStatus);
    }

    @Override
    public int rollBack(int articleId, String backReason,String checkUserName,Integer checkUserId) {
        return articleMapper.rollBack(articleId,backReason,checkUserName,checkUserId);
    }

    @Override
    public Object selectColumn() {
        List<Map<String,Object>> firstColumn = articleMapper.selectColumn();
        for (Map<String,Object> entity:firstColumn){
            entity.put("idStr",String.valueOf(entity.get("id")));
        }
        JSONArray result = ListToTree.listToTree(JSONArray.parseArray( JSON.toJSONString(firstColumn)),"id","parentId","secondColumnList");
        return result;
    }

    @Override
    public Map<String,String> selectById(Integer id,Integer jf) {
        Map<String, String> stringStringMap = articleMapper.selectById(id);
        Set<String> set1 = stringStringMap.keySet();
        Iterator<String> it = set1.iterator();
        if(jf==1){
            while(it.hasNext()) {
                String key = it.next();
                if(key.equals("context") || key.equals("title") ||key.equals("des") || key.equals("articleSource") || key.equals("sendName")){
                    stringStringMap.put(key,ZhConverterUtil.convertToTraditional(stringStringMap.get(key)));
                }
            }
        }
        return stringStringMap;
    }

    @Override
    public Map<String,Object> selectImageSize(Integer columNumber, Integer position) {
        return articleMapper.selectImageSize(columNumber,position);
    }

    @Override
    public List<Article> findGoodArticle(Integer jf) {
        List<Article> goodArticle = articleMapper.findGoodArticle();
        return goodArticle;
    }
}

package com.project.ccps.service.impl;

import com.project.ccps.dto.SysLogDto;
import com.project.ccps.mapper.SysLogMapper;
import com.project.ccps.model.SysLog;
import com.project.ccps.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    SysLogMapper sysLogMapper;

    @Override
    public void saveSysLog(SysLog sysLog) {
        sysLogMapper.saveSysLog(sysLog);
    }

    @Override
    public Map<String,Object> selectAll(SysLogDto sysLogDto) {
        Map<String,Object> result = new HashMap<>();
        int size = sysLogDto.getSize();
        int total = sysLogMapper.selectAllCount(sysLogDto);
        int pageSize = (total/size);
        int remainder = total%size;
        if(remainder > 0 && remainder < size){
            pageSize = (total/size) + 1;
        }
        int startPage = (sysLogDto.getPage() - 1) * size;
        int endPage = size;
        sysLogDto.setStartPage(startPage);
        sysLogDto.setEndPage(endPage);
        List<SysLog> articleDtos = sysLogMapper.selectAll(sysLogDto);
        result.put("total",total);//总叶数
        result.put("pageSize",pageSize);//分页数
        result.put("page",sysLogDto.getPage());//页码
        result.put("size",size);//每页展示的条数
        result.put("items",articleDtos);
        return result;
    }
}

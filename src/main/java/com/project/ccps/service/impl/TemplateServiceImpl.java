package com.project.ccps.service.impl;

import com.project.ccps.dto.TemplateDto;
import com.project.ccps.mapper.TemplateMapper;
import com.project.ccps.model.Page;
import com.project.ccps.model.Template;
import com.project.ccps.model.WebsiteViewCount;
import com.project.ccps.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TemplateServiceImpl implements TemplateService {

    @Autowired
    TemplateMapper templateMapper;
    @Autowired
    RedisTemplate redisTemplate;

    @Override
    public Map<String,Object> selectAll(Page page) {
        Map<String,Object> result = new HashMap<>();
        int size = page.getSize();
        int total = templateMapper.selectAllCount();
        int pageSize = (total/size);
        int remainder = total%size;
        if(remainder > 0 && remainder < size){
            pageSize = (total/size) + 1;
        }
        int startPage = (page.getPage() - 1) * size;
        int endPage = size;
        page.setStartPage(startPage);
        page.setEndPage(endPage);
        List<Template> templateList = templateMapper.selectAll(page);
        result.put("total",total);//总叶数
        result.put("pageSize",pageSize);//分页数
        result.put("page",page.getPage());//页码
        result.put("size",size);//每页展示的条数
        result.put("items",templateList);
        return  result;
    }

    @Override
    public int templateDeploy(Template templete) {
        return templateMapper.addTemplateDeploy(templete);
    }

    @Override
    public List<TemplateDto> template1(Integer tempId) {
        //获取模板配置信息
        List<TemplateDto> templateDtoList = (List<TemplateDto>)  redisTemplate.opsForValue().get( "templateDtoList" );
        templateMapper.websiteView();
        return templateDtoList;
    }

    @Scheduled(cron="0/05 * *  * * ?")
    public void templateCache() {
        //获取模板配置信息
        int tempId = 1;
        List<TemplateDto> templateDtoList = templateMapper.template1(tempId);
        for(TemplateDto templateDto:templateDtoList) {
            if (templateDto.getTempType() == 1 ) {
                List<Map<String, Object>> coverImgList = templateMapper.queryCoverImg( templateDto.getThemeId(), templateDto.getLimitCount() );
                templateDto.setCoverImgList( coverImgList );
            }
            if (templateDto.getTempType() == 2) {
                List<Map<String, Object>> articleTitleList = templateMapper.articleTitle( templateDto.getThemeId(), templateDto.getShowNumber() );
                templateDto.setArticleTitleList( articleTitleList );
            }
            if (templateDto.getTempType() == 3){
                List<Map<String, Object>> subjectsList = templateMapper.subjects(  );
                templateDto.setSubjectsList( subjectsList );
            }
        }
        redisTemplate.opsForValue().set( "templateDtoList",templateDtoList );
    }

    @Override
    public WebsiteViewCount websiteViewCount() {
        return templateMapper.websiteViewCount();
    }
}

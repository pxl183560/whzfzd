package com.project.ccps.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.project.ccps.dto.ArticleDto;
import com.project.ccps.dto.RoleAuthDto;
import com.project.ccps.mapper.MenuAuthMapper;
import com.project.ccps.mapper.ThemeMapper;
import com.project.ccps.mapper.UserAuthMapper;
import com.project.ccps.model.*;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.RoleAuthService;
import com.project.ccps.utils.ListToTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RoleAuthServiceImpl implements RoleAuthService {

    @Autowired
    UserAuthMapper userAuthMapper;
    @Autowired
    MenuAuthMapper menuAuthMapper;
    @Autowired
    ThemeMapper themeMapper;

    @Override
    @Transactional
    public int rollAddAuth(RoleAuthDto roleAuthDto) {
        Integer[] menuArr = roleAuthDto.getMenuArr();
        Integer[] themeArr = roleAuthDto.getThemeArr();
        int roleId = roleAuthDto.getRoleId();
        if(menuArr != null && menuArr.length > 0){
            addAuth(menuArr,"role_menu_auth","menuId",roleId);
        }
        if(themeArr != null && themeArr.length > 0){
            addAuth(themeArr,"role_theme_auth","themeId",roleId);
        }
        int operateRoleValue = userAuthMapper.operateRoleValue(roleId);
        if(roleAuthDto.getOperateAuth() != null){
            OperateAuth operateAuth = roleAuthDto.getOperateAuth();
            operateAuth.setRoleId( roleId );
            if (operateRoleValue > 0){
                return  userAuthMapper.editOperateAuth(operateAuth);
            }
            userAuthMapper.addOperateAuth(operateAuth);
        }
        return 1;
    }


    public void addAuth(Integer[] idArr,String tableName,String columnName,Integer roleId){
        userAuthMapper.delRoleAuth(tableName,roleId);
        for(Integer id:idArr){
            userAuthMapper.addAuth(id,tableName,columnName,roleId);
        }
    }

    @Override
    public Map<String,Object> getUserAuth(int roleId) {
        Map<String,Object> map = new HashMap<>(  );
        //获取菜单权限
        List<MenuAuth> menuAuthList =  menuAuthMapper.queryMenuByRoleId(roleId);
        JSONArray menuAuth = ListToTree.listToTree(JSONArray.parseArray( JSON.toJSONString(menuAuthList)),"id","parentId","children");
        //获取栏目权限
        List<Theme> themeList =  themeMapper.queryThemeByRoleId(roleId);
        JSONArray themeAuth = ListToTree.listToTree(JSONArray.parseArray( JSON.toJSONString(themeList)),"id","parentId","children");
        //操作权限
        List<RoleAuthDto> operateAuth= userAuthMapper.queryOperateByRoleId(roleId);
        map.put("menuAuth",menuAuth);
        map.put("themeAuth",themeAuth);
        map.put("operateAuth",operateAuth);
        return map;
    }

    @Override
    public OperateAuth rollOperateAuth(String roleId) {
        OperateAuth operateAuth = userAuthMapper.rollOperateAuth(roleId);
        operateAuth.setAddStauts(  operateAuth.getAddStauts() > 0 ? 1:0);
        operateAuth.setDelStatus(  operateAuth.getDelStatus() > 0 ? 1:0);
        operateAuth.setEditStatus(  operateAuth.getEditStatus() > 0 ? 1:0);
        operateAuth.setToDraftStatus(  operateAuth.getToDraftStatus() > 0 ? 1:0);
        operateAuth.setSendArticleStatus(  operateAuth.getSendArticleStatus() > 0 ? 1:0);
        operateAuth.setBackOutStatus(  operateAuth.getBackOutStatus() > 0 ? 1:0);
        operateAuth.setRollBackStatus(  operateAuth.getRollBackStatus() > 0 ? 1:0);
        operateAuth.setTopStatus(  operateAuth.getTopStatus() > 0 ? 1:0);
        return operateAuth;
    }

    @Override
    public Role authMap(Role role) {
        List<Integer> themeAuthList = userAuthMapper.themeAuthList(role.getId());
        List<Integer> menuAuthList= userAuthMapper.menuAuthList(role.getId());
        Map<String,Integer> operateAuthMap = userAuthMapper.operateAuthMap(role.getId());

        if(themeAuthList != null){
            role.setThemeAuth( themeAuthList );
        }
        if(menuAuthList != null){
            role.setMenuAuth( menuAuthList);
        }
        if(operateAuthMap != null){
            List<String> operateAuthList = new ArrayList<>(  );
            for(Map.Entry<String,Integer> map:operateAuthMap.entrySet()){
                if(map.getValue() != 0){
                    operateAuthList.add( map.getKey() );
                }
            }
            role.setOperateAuthList( operateAuthList );
        }
        return role;
    }


    @Override
    @Transactional
    public int articleLookAuth(String articleLookStr,int roleId) {
        int value = userAuthMapper.queryLookAuthCount(roleId);
        if(value > 0){
            userAuthMapper.updateLookAuth(articleLookStr,roleId);
        }else{
            userAuthMapper.insertLookAuth(articleLookStr,roleId);
        }
        return ResultEnum.SUCCESS.getCode();
    }
}

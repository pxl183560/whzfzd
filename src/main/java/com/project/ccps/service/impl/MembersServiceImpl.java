package com.project.ccps.service.impl;

import com.project.ccps.mapper.MembersMapper;
import com.project.ccps.model.Members;
import com.project.ccps.service.MembersService;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MembersServiceImpl implements MembersService {
    @Resource
    private MembersMapper mapper;
    @Override
    public void save(Members members) {
        String password=BCrypt.hashpw(members.getPassword(),BCrypt.gensalt());
        members.setPassword(password);
        mapper.save(members);
    }
}

package com.project.ccps.service.impl;

import com.project.ccps.mapper.FileMapper;
import com.project.ccps.service.FileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {
    @Resource
    FileMapper fileMapper;

    @Override
    public String selectFilePath(String id) {
        return fileMapper.selectFilePath(id);
    }

    @Override
    public void delFile(String id) {
        fileMapper.delFile(id);
    }

    @Override
    public long articleMaxTime() {
        return fileMapper.articleMaxTime();
    }

    @Override
    public List<String> articleFileReqUrl() {
        return fileMapper.articleFileReqUrl();
    }
}

package com.project.ccps.service.impl;

import com.project.ccps.mapper.DepManageMapper;
import com.project.ccps.model.DepGroup;
import com.project.ccps.remsg.ResultEnum;
import com.project.ccps.service.DepManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepManageServiceImpl implements DepManageService {

    @Autowired
    DepManageMapper depManageMapper;

    @Override
    public int addDepGroup(DepGroup depGroup) {
        int value = depManageMapper.queryGroupByName(depGroup.getGroupName());
        if(value > 0){
            return ResultEnum.GROUPREPEAT.getCode();
        }
        return depManageMapper.addDepGroup(depGroup);
    }

    @Override
    public List<DepGroup> queryDepGroup(String groupName) {
        return depManageMapper.queryDepGroup(groupName);
    }
}

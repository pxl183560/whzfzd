package com.project.ccps.service;


import com.project.ccps.model.Article;
import com.project.ccps.utils.JSONResult;
import com.project.ccps.utils.Response;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public interface MulitimediaService {
    JSONResult  upload(MultipartFile multipartFile, HttpServletRequest request);

    int uploadFilesOriginal(List<MultipartFile> files) throws IOException;

    Response aliyunImageUpload(MultipartFile[] file);
}

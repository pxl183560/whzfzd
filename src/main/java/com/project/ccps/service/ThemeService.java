package com.project.ccps.service;

import com.alibaba.fastjson.JSONArray;
import com.project.ccps.dto.ThemeDto;
import com.project.ccps.model.Theme;

import java.util.List;

public interface ThemeService {

    int  create(Theme theme);

    int edit(Theme firstTheme);

    int delete(Integer id);

    JSONArray selectAll( String roleId);

    JSONArray selectSiteTheme();

    JSONArray  selectByThemeId(int themeId);
}

package com.project.ccps.service;

import com.project.ccps.model.DepGroup;

import java.util.List;

public interface DepManageService {

    int addDepGroup(DepGroup depGroup);

    List<DepGroup> queryDepGroup(String groupName);
}

package com.project.ccps.service;

import com.project.ccps.dto.RoleAuthDto;
import com.project.ccps.model.OperateAuth;
import com.project.ccps.model.Page;
import com.project.ccps.model.Role;

import java.util.Map;

public interface RoleAuthService {

    Map<String,Object> getUserAuth(int roleId);

    int rollAddAuth(RoleAuthDto roleAuthDto);

    OperateAuth rollOperateAuth(String roleId);

    Role authMap(Role role);

    int articleLookAuth(String articleLookStr,int roleId);

}

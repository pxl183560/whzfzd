package com.project.ccps.service;


import com.project.ccps.dto.SysLogDto;
import com.project.ccps.model.SysLog;

import java.util.Map;

public interface SysLogService {

    void saveSysLog(SysLog sysLog);

    Map<String,Object> selectAll(SysLogDto sysLogDto);
}

package com.project.ccps.service;

import com.alibaba.fastjson.JSONArray;
import com.project.ccps.model.MenuAuth;

public interface MenuAuthService {

    int  create(MenuAuth menuAuth);

    int edit(MenuAuth menuAuth);

    int delete(Integer id);

    JSONArray selectAll(String roleId);
}

package com.project.ccps.service;

import com.project.ccps.dto.TemplateDto;
import com.project.ccps.model.Page;
import com.project.ccps.model.Template;
import com.project.ccps.model.WebsiteViewCount;

import java.util.List;
import java.util.Map;

public interface TemplateService {

    Map<String,Object>  selectAll(Page page);

    int templateDeploy(Template templete);

    List<TemplateDto> template1(Integer tempId);

    WebsiteViewCount websiteViewCount();
}

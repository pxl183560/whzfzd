package com.project.ccps.model;

import lombok.Data;

@Data
public class User {
    private Integer id; //人员id
    private String userName;//人员姓名
}

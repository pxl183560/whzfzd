package com.project.ccps.model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class FileEntity {
    private String type;
    private String size;
    private String path;
    private String titleOrig;
    private String titleAlter;
    private Timestamp uploadTime;
    private String logoRealPath;

}
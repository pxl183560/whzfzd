package com.project.ccps.model;

import lombok.Data;

import java.util.List;

@Data
public class Comment {

    private Integer id;
    private String commContent;//评论内容
    private String commUserName;//评论人
    private String replyUserName;//回复人
    private String articleId;//文章id
    private String commTime;//评论时间
    private List<Reply> replyList;//回复
}

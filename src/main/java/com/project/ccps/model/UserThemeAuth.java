package com.project.ccps.model;

import lombok.Data;

@Data
public class UserThemeAuth {
   private Integer id;
   private Integer roleId;//角色id
   private Integer themeId;//主题id
   private String createTime;//创建时间
   private String  updateTime;//更新时间
}

package com.project.ccps.model;

import lombok.Data;

@Data
public class Page {
    private Integer startPage;
    private Integer endPage;
    private Integer size = 10;
    private Integer page = 1;
}

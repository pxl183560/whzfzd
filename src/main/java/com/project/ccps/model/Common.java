package com.project.ccps.model;

import lombok.Data;

@Data
public class Common {
    private Integer pageNumber;
    private Integer startPage;
    private Integer endPage;
    private String value;
}

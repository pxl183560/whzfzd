package com.project.ccps.model;

import lombok.Data;

@Data
public class WebsiteViewCount {

    private Integer historyViewCount;//历史访问量
    private Integer todayViewCount;//当天访问量
    private Integer yearViewCount;//年度访问量

}

package com.project.ccps.model;

import lombok.Data;

@Data
public class UserRole {
    private Integer id;
    private Integer userid;
    private Integer roleid;
}

package com.project.ccps.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class Theme {

    private Integer id;
    private Integer roleId;//角色id
    private String idStr;
    private String userName;//用户名称
    private Integer parentId = 0;//父级id
    @NotNull(message="一级主题不能为空")
    private String name;//主题名称
    private String englishName;//主题英文名称
    private String describe;//主题描述
    private String subordinateProject;//项目所属栏目
    private Integer ifCheck = 0;//是否审核（0：待审核，1：发布）
    private List<Integer> checkUserId;//审批人id
    private List<String> checkUserName;//审批人名称
    private String checkUserStr;//审批人名称
    private List<Theme> children;//孩子节点
    private Integer themeType;//栏目类型 （0：普通栏目，1：专题栏目）
    private String coverImageUrl;//栏目主题封面
    private Integer articleType;//文章类型（0：普通文章，1：图片文章，2：视频文章）
    private Integer ifComment;//是否可评论(0：否，1：是)
    private Integer priority;//优先级（数值越大优先级别最大）

}

package com.project.ccps.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysLog implements Serializable {

    private Integer id;
    private String userName;
    private String operation;
    private Integer time;
    private String method;
    private String params;
    private String ip;
    private String createTime;

}

package com.project.ccps.model;

import lombok.Data;

@Data
public class OperateAuth {
    /*
    * 0：否
    * 1：是
    * */
    private Integer id;
    private Integer roleId;//角色id
    private Integer editStatus = 0;//编辑
    private Integer delStatus = 0;//删除
    private Integer addStauts = 0;//添加
    private Integer toDraftStatus = 0;//转稿
    private Integer sendArticleStatus = 0;//发稿
    private Integer backOutStatus = 0;//撤销
    private Integer rollBackStatus = 0;//回退
    private Integer topStatus = 0;//置顶
    private String  createTime ;//创建时间
    private String  updateTime;//更新时间

}

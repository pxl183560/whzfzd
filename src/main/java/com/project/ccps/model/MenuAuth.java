package com.project.ccps.model;

import lombok.Data;

@Data
public class MenuAuth {

    private Integer id; //id
    private String menuName;//菜单名称
    private Integer parentId = 0;//父级id
    private String pageUrl;//页面url
    private String description;//描述
    private Integer sortIndex;//菜单排序
    private String menuLogoUrl;//菜单logoUrl
    private Integer[] roleArr;//角色id
    private String createTime;//创建时间
    private String updateTime;//更新时间

}

package com.project.ccps.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Article extends Page{

   private Integer id;//id
   @NotNull(message = "栏目id不能为空")
   private Integer themeId;//栏目id
   @NotNull(message = "文章标题不能为空")
   private String  title;//文章标题
   @NotNull(message = "文章内容不能为空")
   private String content;//文章标题
   private Integer userId;//发稿人id
   private String sourceThemeName;//源栏目名称
   private Integer countyId;//接到id
   private String countyName;//组织名称
   private String userName;//发稿人名称
   private Integer checkUserId;//审稿人id
   private String checkUserName;//审稿人名称
   private String fileReqUrl = "";//文件请求路径
   private String accessoryUrl = "";//附件请求路径
   private Integer priority;//优先级
   private String coverImageUrl;//封面图片
   private String coverVideoUrl;//封面视频
   private Integer flag;//是否封面展示(0:展示图片，1,不展示图片)
   private Integer position;//位置
   private Integer width;//宽度
   private Integer height;//高度
   private Integer checkStatus;//0 待审核 1 审核中，2已发布 ,3回退稿件,4,暂存
   private Integer sortIndex;//1首页显示 0 非首页显示
   private String sendTime;//发布时间
   private String backReason;//回退原因
   private String  createTime;//创建时间
   private String updateTime;//更新时间
   private Integer columnNumber;

}
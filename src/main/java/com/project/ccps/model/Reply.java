package com.project.ccps.model;

import lombok.Data;

@Data
public class Reply {
    private Integer id;
    private Integer commentId;//评论id
    private String replyContent;//回复内容
    private String replyUser;//评论回复人
    private String replyToUser;//回复谁
    private String replyTime;//回复时间

}

package com.project.ccps.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Role {
    private  Integer id;
    private String  roleName;//角色
    private List<Integer> themeAuth;
    private List<Integer> menuAuth;
    private List<String> operateAuthList = new ArrayList<>(  );

}

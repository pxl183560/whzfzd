package com.project.ccps.model;

import lombok.Data;


@Data
public class Template {

    private Integer id;//id
    private Integer position;//位置
    private Integer tempType;//模板类型
    private Integer imgCountLimit;//图片显示数量
    private Integer showNumber;//图片显示数量
    private Integer width;//宽度
    private Integer height;//高度

}
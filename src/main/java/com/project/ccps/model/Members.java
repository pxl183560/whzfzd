package com.project.ccps.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Members {
    private Integer id;
    //用户名
    @NotNull(message = "用户名不能为空")
    private String userName;
    //密码
    @NotNull(message = "用户密码不能为空")
    private String password;
    //邮箱
    private String email;
    //电话
    private String phone;
    //类型
    private Integer type;
    //单位
    private String unit;
    //地址
    private String address;
    //中文名
    private String name;
    //头像
    private String avatar;
    //简介
    private String description;
}

package com.project.ccps.model;

import lombok.Data;

@Data
public class DepGroup {
   private Integer id;
   private String groupName;//组名称
   private String description;//描述
   private String createTime;//创建时间
   private String updateTime;//更新时间
}
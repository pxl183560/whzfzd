package com.project.ccps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


@SpringBootApplication
@ServletComponentScan
public class CcpsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CcpsApplication.class, args);
    }

}

package com.project.ccps.dto;

import lombok.Data;

@Data
public class ThemeDto {
    private Integer id;
    private Integer parentId;
    private String name;
}

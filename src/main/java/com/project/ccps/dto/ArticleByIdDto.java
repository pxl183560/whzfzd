package com.project.ccps.dto;

import com.project.ccps.model.Comment;
import lombok.Data;

import java.util.List;

@Data
public class ArticleByIdDto {

    private Integer id;
    private Integer frontId;//上一级id
    private Integer laterId;//下一级id
    private Integer themeId;//栏目id
    private Integer readCount;//阅读数
    private String content;//内容
    private String title;//标题
    private String accessoryUrl;//附件地址
    private Integer ifComment;//是否评论（0：否，1：是）
    private String themeName;//栏目名称
    private String createTime;//创建时间

}

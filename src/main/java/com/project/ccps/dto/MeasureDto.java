package com.project.ccps.dto;

import lombok.Data;

@Data
public class MeasureDto {
    private Integer  length;
    private Integer  wide;
}

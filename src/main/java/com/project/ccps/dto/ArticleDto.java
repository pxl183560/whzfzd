package com.project.ccps.dto;

import lombok.Data;

@Data
public class ArticleDto {
    private Integer id;//文章id
    private Integer themeId;//栏目id
    private String title;//文章标题
    private String content;//文章内容
    private String sendTime;//发布时间
    private String createTime;//创建时间
    private String updateTime;//更新时间
    private Integer checkStatus;//审核状态(0:未审核，1：已审核)
    private String userName;//发稿人
    private String checkUserName;//审核人
    private String themeName;//主题名称
    private String sourceThemeName;//源主题名称
    private String accessoryUrl;//附件地址
    private Integer top;//置顶：1：是，0：否
    private String flag;//是否为封面  1：是，0：否
    private String backReason;//回退原因
    private String coverImageUrl;//封面图片
    private String coverVideoUrl;//封面视频
}

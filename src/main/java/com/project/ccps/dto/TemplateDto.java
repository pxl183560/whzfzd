package com.project.ccps.dto;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class TemplateDto implements Serializable {

    private Integer themeId;//栏目id
    private Integer parentId;//父级id
    private String parentName;//父级名称
    private String themeName;//栏目名称
    private String position;//位置
    private Integer tempType;//模板类型
    private Integer showNumber;//显示条数
    private Integer limitCount;//图片数量
    private Integer ifComment;//是否可评论(0:否，1：是)
    private List<Map<String,Object>> coverImgList;//封面图片
    private List<Map<String,Object>> articleTitleList;//文章标题
    private List<Map<String, Object>> subjectsList;//栏目主题

}

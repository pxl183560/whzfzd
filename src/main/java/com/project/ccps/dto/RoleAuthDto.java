package com.project.ccps.dto;

import com.project.ccps.model.OperateAuth;
import lombok.Data;

@Data
public class RoleAuthDto {

    private Integer roleId;
    private Integer[] menuArr;
    private Integer[] themeArr;
    private OperateAuth operateAuth;

}

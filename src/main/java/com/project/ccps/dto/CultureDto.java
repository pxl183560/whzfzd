package com.project.ccps.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CultureDto {
    private Integer id;
    @NotNull(message="一级主题不能为空")
    private String name;//一级主题名称
    private String englishName;//一级主题英文名称
    private String describe;//一级主题描述
    private String subordinateProject;
}

package com.project.ccps.dto;

import com.project.ccps.model.Page;
import lombok.Data;

@Data
public class SysLogDto extends Page {

    private String startTime;
    private String endTime;

}

package com.project.ccps.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    private Integer uid;
    /**
     * 用户身份
     */
    private Integer position;
    /**
     * 公司id
     */
    private Integer companyId;

}

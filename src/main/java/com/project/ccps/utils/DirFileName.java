package com.project.ccps.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirFileName {
    public static List<String> fileNames (String fileUrl) {
        List<String> list = new ArrayList<>();
        File file = new File(  fileUrl);
        File[] files = file.listFiles();
        for (File img : files) {
            list.add( img.getName() );
        }
        return list;
    }
}

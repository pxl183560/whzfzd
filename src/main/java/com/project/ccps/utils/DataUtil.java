package com.project.ccps.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class DataUtil {

    public void isomerism(String str) {
        synchronized (this) {
            try {
                System.out.println("start");
//                String windowcmd = "cmd /c python datax.py D:\\DataX\\datax\\job\\job5.json";
                String windowcmd = "cmd /c python datax.py D:\\DataX\\datax\\job\\" + str;
                System.out.println(windowcmd);
                //.exec("你的命令",null,new File("datax安装路径"));
                Process pr = Runtime.getRuntime().exec(windowcmd, null, new File("D:\\DataX\\datax\\bin"));
                BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                String line = null;
                while ((line = in.readLine()) != null) {
                    System.out.println(line);
                }
                in.close();
                pr.waitFor();
                System.out.println("end");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


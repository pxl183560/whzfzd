package com.project.ccps.utils;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class UrlChange {

    /**
     * 将文本中的相对地址转换成对应的绝对地址
     * @return
     */
    public static List<String> getFileList() {
        File file = new File("G:/NewsImages");
        File[] fileList = file.listFiles();
        List<String> fileNamelist = new ArrayList<>(  );
        for (int i = 0; i < fileList.length; i++) {
            if (fileList[i].isFile()) {
                String fileName = fileList[i].getName();
                System.out.println("文件：" + fileName);
            }
            if (fileList[i].isDirectory()) {
                String fileName = fileList[i].getName();
                fileNamelist.add( fileName );
            }
        }
        return  fileNamelist;
    }

    public static String processImgSrc(String content,String dir) {
        content = content.replaceAll( "/NewsImages/"+dir, "http://10.10.11.138:8011/imageFile" );
        return content;
    }

    @Test
    public void test(){
        List<String> fileNamelist =  UrlChange.getFileList();
        System.out.println(fileNamelist);
    }
}

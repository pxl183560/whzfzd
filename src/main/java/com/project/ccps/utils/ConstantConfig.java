package com.project.ccps.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Data
@Configuration
@Component
public class ConstantConfig {
    @Value("${aliyun.endpoint}")
    private   String END_POINT;
    @Value("${aliyun.keyid}")
    private  String ACCESS_KEY_ID;
    @Value("${aliyun.keysecret}")
    private  String ACCESS_KEY_SECRET;
    @Value("${aliyun.filehost}")
    private  String FILE_HOST;
    @Value("${aliyun.bucketname}")
    private  String BUCKET_NAME;
    @Value("${aliyun.url}")
    private String url;
}

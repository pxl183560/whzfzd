package com.project.ccps.utils;

import net.sf.json.JSONObject;
import org.junit.Test;

public class MapUtil {

    public static String  getLocationInfo(String lat1, String lng) {
        String [] api = new String[]{"d7d3c393aa44f94728afdd0e2c14610d","31140107ee72659278c19dfe44f34540","0174f3e3d2ea0fe7d54a53d80c3faaa0",
                "810dc484bb0d9e0cf1ca3f4edbad8498","b1e19af1c3a39191d6ef2de1b6d79a49",
                "6339504d7ff0dcebb3d09c888ab58ebf"	,
                "db9139594adc8d94a984023fd1dd89b2",
                "dad26d7faf787e743bd434d0e5d13719",
                "097df3feffcbc23d039edfc181fc0b2f",
                "441d8ce68515160e1a5d0b65fe5211e5"
        };
        String address = "";
        for(int i = 0;i<api.length;i++){
            String url ="http://restapi.amap.com/v3/geocode/regeo?output=JSON&location="+lng+","+lat1+"&key="+api[i]+"&radius=0&extensions=base";
            address =  HttpUtil.getRequest(url);
            if(address != null || address != ""){
                continue;
            }
        }
        JSONObject obj = JSONObject.fromObject(address);
        String township = JSONObject.fromObject(JSONObject.fromObject(obj.get("regeocode")).get("addressComponent")).get("township").toString();
        return township;
    }

    @Test
    public void mapTest(){
       String value =  MapUtil.getLocationInfo("31.035914","121.659152");
        System.out.println(value);
    }
}

package com.project.ccps.utils;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class EngineeCore {

    String filePath = "E:\\voice\\voice_cache.wav";

    AudioFormat audioFormat;
    TargetDataLine targetDataLine;
    boolean flag = true;
    static ByteArrayInputStream bais = null;
    static ByteArrayOutputStream baos = new ByteArrayOutputStream();
    AudioInputStream ais = null;
    File audioFile = new File(filePath);

    int beak = 0;

    private void stopRecognize() {
        flag = false;
        targetDataLine.stop();
        targetDataLine.close();
    }
    private AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        // 8000,11025,16000,22050,44100
        int sampleSizeInBits = 16;
        // 8,16
        int channels = 1;
        // 1,2
        boolean signed = true;
        // true,false
        boolean bigEndian = false;
        // true,false
        return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
    }// end getAudioFormat


    public void startRecognize() {
        Thread.currentThread().interrupt();
        try {
            audioFormat = getAudioFormat();
            DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, audioFormat);
            targetDataLine = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
            flag = true;
            new CaptureThread().interrupt();
            new CaptureThread().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  String  writeVoice(){
        beak = 1;
        try {
            //取得录音输入流
            audioFormat = getAudioFormat();
            byte audioData[] = baos.toByteArray();
            bais = new ByteArrayInputStream(audioData);
            ais = new AudioInputStream(bais, audioFormat, audioData.length / audioFormat.getFrameSize());
            //定义最终保存的文件名
            System.out.println("开始生成语音文件");
            AudioSystem.write(ais, AudioFileFormat.Type.WAVE, audioFile);
            baos = new ByteArrayOutputStream();
            //stopRecognize();
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }finally {
            //关闭流
            try {
                ais.close();
                bais.close();
                baos.reset();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "success";
    }
    class CaptureThread extends Thread {
        public void run() {
            AudioFileFormat.Type fileType = null;
            fileType = AudioFileFormat.Type.WAVE;
            //声音录入的权值
            int weight = 2;
            //判断是否停止的计数
            int downSum = 0;
            try {
                targetDataLine.open(audioFormat);
                targetDataLine.start();
                byte[] fragment = new byte[1024];

                ais = new AudioInputStream(targetDataLine);
                while (flag) {
                    targetDataLine.read(fragment, 0, fragment.length);
                    //当数组末位大于weight时开始存储字节（有声音传入），一旦开始不再需要判断末位
                    if (Math.abs(fragment[fragment.length - 1]) > weight || baos.size() > 0) {
                        baos.write(fragment);
                        //判断语音是否停止
//                        if (Math.abs(fragment[fragment.length - 1]) <= weight) {
//                            downSum++;
//                        } else {
//                            downSum = 0;
//                        }
                        System.out.println(Thread.currentThread().getName());
                        if (beak ==1) {
                            System.out.println("停止录入");
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
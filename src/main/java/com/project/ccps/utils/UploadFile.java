package com.project.ccps.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UploadFile {
    public static List<String> ImageUpload(List<MultipartFile> files) {
        String path = "F:\\image\\";
        List<String> msgs = new ArrayList<>();
        if (files.size() < 1) {
            msgs.add("file_empty");
            return msgs;
        }
        for (int i = 0; i < files.size(); i++) {
            MultipartFile file = files.get(i);
            if (!file.isEmpty()) {
                String filename = file.getOriginalFilename();
                String type = filename.indexOf(".") != -1
                        ? filename.substring(filename.lastIndexOf("."), filename.length())
                        : null;
                if (type == null) {
                    msgs.add("file_empty");
                    return msgs;
                }

                if (!(".PNG".equals(type.toUpperCase()) || ".JPG".equals(type.toUpperCase()))) {
                    msgs.add("wrong_file_extension");
                    return msgs;
                }
            }
        }
        for (int i = 0; i < files.size(); i++) {
            MultipartFile file = files.get(i);
            String filename = file.getOriginalFilename();
            String type = filename.indexOf(".") != -1 ? filename.substring(filename.lastIndexOf("."), filename.length())
                    : null;
            String filepath = path + new Date().getTime()+ type;
            File filesPath = new File(path);
            if (!filesPath.exists()) {
                filesPath.mkdir();
            }
            BufferedOutputStream out = null;
            type = filepath.indexOf(".") != -1 ? filepath.substring(filepath.lastIndexOf(".") + 1, filepath.length())
                    : null;
            try {
                out = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
                out.write(file.getBytes());
                msgs.add(new Date().getTime()+ "."+type);
                out.flush();
                out.close();

            } catch (Exception e) {
                // 没有上传成功
                System.out.println(e);
                e.printStackTrace();
            }
        }
        return msgs;
    }
}

package com.project.ccps.utils;

import com.project.ccps.model.Role;
import com.project.ccps.model.User;
import oracle.jdbc.driver.OracleDriver;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
public class OracleConn {

    public final static List<Role> getRole(String user,String password,String jdbc){
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Role> roleList = new ArrayList<>(  );
        try {
            Driver driver = new OracleDriver();
            DriverManager.deregisterDriver(driver);

            Properties pro = new Properties();
            pro.put("user", user);
            pro.put("password", password);
            connect = driver.connect(jdbc, pro);
            PreparedStatement preState = connect.prepareStatement("select ID,ROLENAME  from TB_ROLE where FLAG = 1");
            resultSet = preState.executeQuery();
            while (resultSet.next()) {
                Role role = new Role();
                int id = resultSet.getInt("ID");
                String roleName = resultSet.getString("ROLENAME");
                role.setId( id );
                role.setRoleName( roleName );
                roleList.add( role );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //第六步：关闭资源
            try {
                if (resultSet!=null) resultSet.close();
                if (statement!=null) statement.close();
                if (connect!=null) connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return roleList;
    }

    public  final static List<User> getUser(String user,String password,String jdbc){
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<User> userList = new ArrayList<>(  );
        try {
            Driver driver = new OracleDriver();
            DriverManager.deregisterDriver(driver);

            Properties pro = new Properties();
            pro.put("user", user);
            pro.put("password", password);
            connect = driver.connect(jdbc, pro);
            String sql = "select PERSON.ID,PERSON.NAME from TB_PERSON PERSON \n" +
                    "LEFT JOIN TB_USEROLE USERROLE ON PERSON.ID = USERROLE.USERID\n" +
                    "LEFT JOIN TB_ROLE ROLE ON USERROLE.ROLEID = ROLE.ID\n" +
                    "WHERE ROLE.ID = 2137 AND ROLE.ID  != 2133";
            PreparedStatement preState = connect.prepareStatement(sql);
            resultSet = preState.executeQuery();
            while (resultSet.next()) {
                User user1 = new User();
                int id = resultSet.getInt("ID");
                String userName = resultSet.getString("NAME");
                user1.setId( id );
                user1.setUserName( userName );
                userList.add( user1 );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //第六步：关闭资源
            try {
                if (resultSet!=null) resultSet.close();
                if (statement!=null) statement.close();
                if (connect!=null) connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userList;
    }

    public final static int backstageAuth(String account,String user,String password,String jdbc){
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int count = 0;
        String sql = "select count(1) from TB_ROLE where id in (" +
                "select roleId from TB_USEROLE where USERID IN (" +
                "select id from TB_PERSON where ACCOUNT = '"+account +"')"+
                ") and FLAG = 1";
        List<Role> roleList = new ArrayList<>(  );
        try {
            Driver driver = new OracleDriver();
            DriverManager.deregisterDriver(driver);

            Properties pro = new Properties();
            pro.put("user", user);
            pro.put("password", password);
            connect = driver.connect(jdbc, pro);
            PreparedStatement preState = connect.prepareStatement(sql);
            resultSet = preState.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //第六步：关闭资源
            try {
                if (resultSet!=null) resultSet.close();
                if (statement!=null) statement.close();
                if (connect!=null) connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return count;
    }
}

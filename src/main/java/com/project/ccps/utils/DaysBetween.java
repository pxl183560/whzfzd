package com.project.ccps.utils;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DaysBetween {
    public long daysBetween(String date1, String  date2) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd");
        Date d1=sdf.parse(date1);
        Date d2=sdf.parse(date2);
        long daysBetween=(d2.getTime()-d1.getTime()+1000000)/(3600*24*1000);
        return daysBetween;
    }
    @Test
    public void test() throws ParseException {
        DaysBetween daysBetween = new DaysBetween();
        String date1="2009-01-00";
        String date2="2010-01-01";
        daysBetween.daysBetween(date1,date2);
    }

}

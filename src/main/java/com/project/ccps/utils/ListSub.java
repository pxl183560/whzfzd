package com.project.ccps.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListSub {
    public <T> Object splitList(List<T> list, int pageSize) {
        Map<String,Object> pageMap = new HashMap<>();

        int listSize = list.size();
        int page = (listSize + (pageSize - 1)) / pageSize;
        pageMap.put("listSize",listSize);
        pageMap.put("page",page);
        pageMap.put("pageSize",pageSize);

        List<List<T>> listArray = new ArrayList<List<T>>();
        for (int i = 0; i < page; i++) {
            List<T> subList = new ArrayList<T>();
            for (int j = 0; j < listSize; j++) {
                int pageIndex = ((j + 1) + (pageSize - 1)) / pageSize;
                if (pageIndex == (i + 1)) {
                    subList.add(list.get(j));
                }
                if ((j + 1) == ((j + 1) * pageSize)) {
                    break;
                }
            }
            listArray.add(subList);
        }
        pageMap.put("listArray",listArray);
        return pageMap;
    }
}

package com.project.ccps.utils;

import lombok.Data;

@Data
public class Response<T> {

    private Integer code = 0;

    private String msg = "success";

    private T data;

    public Response(T data) {
        this.data = data;
    }

    public static Response isFail() {
        return new Response().code(1).msg("失败");
    }

    public Response msg(Throwable e) {
        this.setMsg(e.toString());
        return this;
    }
    public Response(){
    }
    public Response(String msg, Integer code, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Response<T> code(int code) {
        this.setCode(code);
        return this;
    }
    public Response<T> msg(String msg){
        this.setMsg(msg);
        return this;
    }
}

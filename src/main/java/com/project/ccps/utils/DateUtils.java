package com.project.ccps.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author cmf
 *
 */
public class DateUtils {
	private static final SimpleDateFormat SDF_DATE = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat SDF_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String dateTimeToString(Date date) {
		return SDF_DATE_TIME.format(date);
	}

	public static Date stringToDateTime(String dateStr) throws ParseException {
		return SDF_DATE_TIME.parse(dateStr);
	}

	public static String dateToString(Date date) {
		return SDF_DATE.format(date);
	}

	public static Date stringToDate(String dateStr) throws ParseException {
		return SDF_DATE.parse(dateStr);
	}
	public static String UTCStringtODefaultString(String UTCString) {
		try
		{
			if (UTCString!=null) {
				UTCString = UTCString.replace("Z", " UTC");
				SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
				SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				TimeZone tz = TimeZone.getTimeZone("ETC/GMT-8");
				TimeZone.setDefault(tz);
				defaultFormat.setTimeZone(tz);
				Date date = utcFormat.parse(UTCString);
				return defaultFormat.format(date);
			}else{
				return null;
			}

		} catch(Exception pe)
		{
			return null;
		}
	}
}

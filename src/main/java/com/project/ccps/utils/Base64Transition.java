package com.project.ccps.utils;

import java.io.*;

public class Base64Transition {
    //图片转化成base64字符串
    // 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
    public static String GetImageStr(String imgFile) {
        InputStream in = null;
        byte[] data = null;
        //读取图片字节数组  
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        //对字节数组Base64编码  
        return Base64Tools.encode(data);
    }
    //base64字符串转化成图片
    //对字节数组字符串进行Base64解码并生成图片
    public static boolean GenerateImage(String imgStr,String imgFilePath) {
        //图像数据为空
        if (imgStr == null)
            return false;
        try {
            //Base64解码  
            byte[] b = Base64Tools.decode(imgStr);
            for(int i=0;i<b.length;++i) {
                //调整异常数据
                if(b[i]<0) {
                    b[i]+=256;
                }
            }
            //生成jpeg图片  
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
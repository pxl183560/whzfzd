package com.project.ccps.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageUtil {
    /**
     * 开始分页
     * @param list
     * @param pageNum 页码
     * @param pageSize 每页多少条数据
     * @return
     */
    public static Map<String,Object> startPage(List list, Integer pageNum, Integer pageSize) {
        Map<String,Object> map = new HashMap<>(  );
        if (list == null) {
            return null;
        }
        if (list.size() == 0) {
            return null;
        }

        Integer count = list.size(); // 记录总数
        Integer pageCount = 0; // 页数
        if (count % pageSize == 0) {
            pageCount = count / pageSize;
        } else {
            pageCount = count / pageSize + 1;
        }

        int fromIndex = 0; // 开始索引
        int toIndex = 0; // 结束索引

        if (pageNum != pageCount) {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = fromIndex + pageSize;
        } else {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = count;
        }

        List pageList = list.subList(fromIndex, toIndex);
        map.put("total",count);//总叶数
        map.put("pageSize",pageCount);//分页数
        map.put("page",pageNum);//页码
        map.put("size",pageSize);//每页展示的条数
        map.put("items",pageList);
        return map;
    }
}

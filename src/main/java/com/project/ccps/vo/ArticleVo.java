package com.project.ccps.vo;

import com.project.ccps.model.Page;
import lombok.Data;

@Data
public class ArticleVo extends Page {

    private Integer themeId;//栏目id
    private String topArticleId;//置顶文章id
    private String  themeIdStr;//多个栏目id
    private String searchContent;//查询内容
    private String userAccount;//用户账户
    private Integer checkStatus;//审核状态
    private Integer userId;//用户id
    private String roleId;//角色id
    private String startTime;//开始时间
    private String endTime;//结束时间
    private Integer flag;//区分角色（管理员角色默认设置1）
    private Integer ifRecycled;//是否回收站（0:否，1：是）
    private String timeParam;//时间参数（查询限定时间）

}

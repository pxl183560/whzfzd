package com.project.ccps.remsg;

import com.project.ccps.utils.JSONResult;

public class ResultValue {
    public static JSONResult methodResult(int reusltValue){
        JSONResult jsonResult = new JSONResult(  );
        for (ResultEnum resultEnum : ResultEnum.values()) {
            if(resultEnum.getCode() == reusltValue){
                jsonResult.setMessage( resultEnum.getMsg() );
                jsonResult.setStatusCode( resultEnum.getCode() );
                return jsonResult;
            }
        }
        jsonResult.setStatusCode( ResultEnum.SUCCESS.getCode());
        jsonResult.setMessage( ResultEnum.SUCCESS.getMsg());
        return jsonResult;
    }
}

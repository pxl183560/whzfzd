package com.project.ccps.remsg;

public enum ResultEnum {
    //成功
    SUCCESS(200, "操作成功"),
    //栏目管理
    COLUMNREPEAT(201,"对不起,栏目不允许重名"),
    NEEDCHECK(202,"对不起,该栏目已设置成需审核状态，请设置无需审核状态再添加子级栏目"),
    ARTICENODE(203,"对不起,该栏目存在文章，不允许添加子集栏目"),
    //栏目管理
    MENUREPEAT(701,"对不起,菜单名称不允许重复"),
    EXITSUB(702,"存在子集栏目,请在子集栏目下设置审批人"),
    //部门管理
    GROUPREPEAT(301, "对不起,组名不允许重复"),
    //文章管理
    ARTICLEREPEAT(601,"对不起,同一栏目下文章标题不允许重名"),
    CHOOSECOLUMN(602,"请选择栏目"),
    CHILDNODE(603,"对不起,该栏目存在子级栏目,请在子级栏目添加文章"),
    IDISNULL(604,"请选择文章进行删除"),
    //公共异常
    RUNEXECEPTION(500, "运行时异常"),
    COLUMNSUB(101, "对不起,该栏目存在子集栏目不允许删除"),
    ARTICLESUB(102, "对不起,该栏目存在关联文章不允许删除"),
    MENUSUB(103,"对不起，该菜单存在子集菜单不允许删除"),
    UNDEFINE(-1, "未定义异常信息");

    private int code;
    private String msg;
    ResultEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public static String msg(int code) {
        for (ResultEnum m : ResultEnum.values()) {
            if (m.getCode() == code) {
                return m.getMsg();
            }
        }
        return UNDEFINE.getMsg();
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
}

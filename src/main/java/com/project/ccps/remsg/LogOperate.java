package com.project.ccps.remsg;

public class LogOperate {

    //栏目操作名称
    public final static String THEME_ADD = "栏目添加";
    public final static String THEME_EDIT = "栏目编辑";
    public final static String THEME_DELETE = "栏目删除";

    //文章操作名称
    public final static String ARTICLE_ADD = "文章添加";
    public final static String ARTICLE_EDIT = "文章编辑/发布";
    public final static String ARTICLE_DELETE = "文章删除";
    public final static String ARTICLE_PRIORITY = "文章置顶";
    public final static String ARTICLE_REVOCATION = "文章撤回";
    public final static String ARTICLE_TRANS = "转发稿件";
    public final static String ARTICLE_BACKOUT = "撤销/取消撤销稿件";
    public final static String ARTICLE_ROLLBACK = "回退稿件";

}
